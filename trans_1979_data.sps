* Commande pour l'importation du fichier CSV dans SPSS

* Attention: modifier la ligne ci-dessous commençant par /FILE
* afin d'indiquer le chemin complet du jeu de données
* (p.ex. C:\Users\FooBar\trans_1979_data.csv)

* Importation du fichier

SET UNICODE=ON.
GET DATA
 /TYPE=TXT
 /ENCODING='UTF8'
 /FILE='trans_1979_data.csv'
 /DELCASE=LINE
 /DELIMITERS=","
 /ARRANGEMENT=DELIMITED
 /FIRSTCASE=2
 /IMPORTCASE=ALL
 /VARIABLES=
 v1 F6
 v2 F2
 v3 F2
 v4 F2
 q1 F2
 q1_type F2
 q2 F2
 q3 F4
 q3_age F2
 q3_const F2
 q4 F2
 q4_regro F2
 q5 F2
 q6 F4
 q6v1 F4
 q6v2 F4
 q6d1 F4
 q6d2 F4
 q6r1 F4
 q6r2 F4
 q7 F4
 q8 F2
 q8_nombr F2
 q9_1sex F2
 q9_1nais F4
 q9_1dec F4
 q9_1domi F2
 q9_1prof F2
 q9_1pr_1 F2
 q9_1peti F2
 q9_2sex F2
 q9_2nais F4
 q9_2dec F4
 q9_2domi F2
 q9_2prof F2
 q9_2pr_1 F2
 q9_2peti F2
 q9_3sex F2
 q9_3nais F4
 q9_3dec F4
 q9_3domi F2
 q9_3prof F2
 q9_3pr_1 F2
 q9_3peti F2
 q9_4sex F2
 q9_4nais F4
 q9_4dec F4
 q9_4domi F2
 q9_4prof F2
 q9_4pr_1 F2
 q9_4peti F2
 q9_5sex F2
 q9_5nais F4
 q9_5dec F4
 q9_5domi F2
 q9_5prof F2
 q9_5pr_1 F2
 q9_5peti F2
 q9_6sex F2
 q9_6nais F4
 q9_6dec F4
 q9_6domi F2
 q9_6prof F2
 q9_6pr_1 F2
 q9_6peti F2
 q9_7sex F2
 q9_7nais F4
 q9_7dec F4
 q9_7domi F2
 q9_7prof F2
 q9_7pr_1 F2
 q9_7peti F2
 q9_8sex F2
 q9_8nais F4
 q9_8dec F4
 q9_8domi F2
 q9_8prof F2
 q9_8pr_1 F2
 q9_8peti F2
 q9_9sex F2
 q9_9nais F4
 q9_9dec F4
 q9_9domi F2
 q9_9prof F2
 q9_9pr_1 F2
 q9_9peti F2
 q10 F2
 q11a F2
 q11b F2
 q12a F2
 q12b F2
 q13a F2
 q13b F2
 q14 F2
 q15a F2
 q15b F2
 q15c F2
 q15d F2
 q15e F2
 q15f F2
 q15g F2
 q15h F2
 q16 F2
 q17 F2
 q18a F2
 q18b F2
 q18c F2
 q18d F2
 q18e F2
 q18f F2
 q19a F2
 q19b F4
 q20 F2
 q21 F2
 q22a F2
 q22b F2
 q22c F2
 q22d F2
 q22e F2
 q23a F2
 q23b F2
 q23c F2
 q23d F2
 q24a F2
 q24b F2
 q24c F2
 q24d F2
 q25a F2
 q25b F2
 q25c F2
 q25d F2
 q25e F2
 q25f F2
 q25g F2
 q25h F2
 q25i F2
 q25j F2
 q25k F2
 q26a F2
 q26b F2
 q27a F2
 q27b F2
 q27c F2
 q27d F2
 q27e F2
 q27f F2
 q27g F2
 q27h F2
 q27i F2
 q27j F2
 q27k F2
 q27l F2
 q27m F2
 q28 F2
 q28_mens F2
 q29a F2
 q29b F2
 q29c F2
 q29d F2
 q29e F2
 q29f F2
 q29g F2
 q29h F2
 q29i F2
 q29j F2
 q30 F2
 q31 F2
 q32 F2
 q33 F2
 q34 F2
 q34a F2
 q34b F2
 q35 F2
 q36 F2
 q37a F2
 q37b F2
 q38a F2
 q38b F2
 q38c F2
 q38d F2
 q38e F2
 q38f F2
 q38g F2
 q39a F2
 q39b F2
 q39c F2
 q39d F2
 q39e F2
 q40a F2
 q40b F2
 q40c F2
 ie1 F2
 ie3 F2
 i1_age F2
 i2 F2
 i3 F2
 i4 F2
 i4a F2
 i4b F2
 i4c F2
 i4d F2
 i4e F2
 i5a F2
 i5ba F2
 i5bb F2
 i5bc F2
 i5bd F2
 i5be F2
 i5bf F2
 i5bg F2
 i6a F2
 i6b F2
 i7 F2
 i8 F2
 i9 F2
 i10_annee F4
 i10_comm F2
 i10_cant F2
 i11 F2
 i12a F2
 i12b F2
 i12c F2
 i12d F2
 i13 F2
 i14a F2
 i14b F2
 i14c F2
 i14d F2
 i14e F2
 i14f F2
 i15a F2
 i15b F2
 i15c F2
 i15d F2
 i15e F2
 i15f F2
 i15g F2
 i15h F2
 i15i F2
 i15j F2
 i15k F2
 i16 F2
 i17a F2
 i17b F2
 i18a F2
 i18b F2
 i18c F2
 i18d F2
 i19 F2
 i19a F2
 i19b F2
 i19c F2
 i19d F2
 i20 F2
 i20a F2
 i20b F2
 i20c F2
 i21a F2
 i21b F2
 i21c F2
 i21d F2
 i21e F2
 i21f F2
 i22a F2
 i22b F2
 i23a F2
 i23b F2
 i23c F2
 i23d F2
 i24 F2
 i24_age F2
 i25 F2
 i25_age F2
 i26 F2
 i26b F2
 i27a F2
 i27b F2
 i27c F2
 i27d F2
 i27e F2
 i28a F2
 i28b F2
 i28c F2
 i28d F2
 i28e F2
 i29a F2
 i29b F2
 i30a F2
 i30b F2
 i30c F2
 i30d F2
 i30e F2
 i30f F2
 i31a F2
 i31b F2
 i31c F2
 i31d F2
 i31e F2
 i31f F2
 i32 F2
 i33 F2
 i34_prin F2
 i34_seco F2
 i35 F2
 i36 F2
 i37a F2
 i37b F2
 i37c F2
 i37d F2
 i37e F2
 i37f F2
 i37g F2
 i37h F2
 i38 F2
 i38a F2
 i38b F2
 i38c F2
 i38d F2
 i38e F2
 i38f F2
 i38g F2
 i38h F2
 i39 F2
 i39_anne F4
 i39_repr F2
 i39_prof F2
 i39_pr_1 F2
 i39_pr_2 F2
 i39_pr_3 F2
 i40 F4
 i41 F2
 i43 F2
 i44a F2
 i44b F2
 i45 F2
 i46 F2
 i47a1 F2
 i47a2 F2
 i47a3 F2
 i47a4 F2
 i47a5 F2
 i47a6 F2
 i47b1 F2
 i47b2 F2
 i47b3 F2
 i47b4 F2
 i47b5 F2
 i47b6 F2
 i47c1 F2
 i47c2 F2
 i47c3 F2
 i47c4 F2
 i47c5 F2
 i47c6 F2
 i47d1 F2
 i47d2 F2
 i47d3 F2
 i47d4 F2
 i47d5 F2
 i47d6 F2
 i47e1 F2
 i47e2 F2
 i47e3 F2
 i47e4 F2
 i47e5 F2
 i47e6 F2
 i47f1 F2
 i47f2 F2
 i47f3 F2
 i47f4 F2
 i47f5 F2
 i47f6 F2
 i47g1 F2
 i47g2 F2
 i47g3 F2
 i47g4 F2
 i47g5 F2
 i47g6 F2
 i47h1 F2
 i47h2 F2
 i47h3 F2
 i47h4 F2
 i47h5 F2
 i47h6 F2
 i47i1 F2
 i47i2 F2
 i47i3 F2
 i47i4 F2
 i47i5 F2
 i47i6 F2
 i47j1 F2
 i47j2 F2
 i47j3 F2
 i47j4 F2
 i47j5 F2
 i47j6 F2
 i47k1 F2
 i47k2 F2
 i47k3 F2
 i47k4 F2
 i47k5 F2
 i47k6 F2
 i51f F2
 i52a F2
 i52b F2
 i52c F2
 i52d F2
 i52e F2
 i52f F2
 i53 F2
 i53_type F2
 i54a F2
 i54b F2
 i54c F2
 i54d F2
 i54e F2
 i55a F2
 i55b F2
 i55c F2
 i55d F2
 i55e F2
 i55f F2
 i55g F2
 i56a F2
 i56b F2
 i56c F2
 i56d F2
 i56e F2
 i56f F2
 i56g F2
 i57a F2
 i57b F2
 i57c F2
 i57d F2
 i57e F2
 i58 F2
 i59 F2
 i60 F2
 i61 F2
 i62ac_01 F2
 i62av_01 F2
 i62co_01 F2
 i62ac_02 F2
 i62av_02 F2
 i62co_02 F2
 i62ac_03 F2
 i62av_03 F2
 i62co_03 F2
 i62ac_04 F2
 i62av_04 F2
 i62co_04 F2
 i62ac_05 F2
 i62av_05 F2
 i62co_05 F2
 i62ac_06 F2
 i62av_06 F2
 i62co_06 F2
 i62ac_07 F2
 i62av_07 F2
 i62co_07 F2
 i62ac_08 F2
 i62av_08 F2
 i62co_08 F2
 i62ac_09 F2
 i62av_09 F2
 i62co_09 F2
 i62ac_10 F2
 i62av_10 F2
 i62co_10 F2
 i62ac_11 F2
 i62av_11 F2
 i62co_11 F2
 i62ac_12 F2
 i62av_12 F2
 i62co_12 F2
 i62ac_13 F2
 i62av_13 F2
 i62co_13 F2
 i62ac_14 F2
 i62av_14 F2
 i62co_14 F2
 i62ac_15 F2
 i62av_15 F2
 i62co_15 F2
 i62ac_16 F2
 i62av_16 F2
 i62co_16 F2
 i62ac_17 F2
 i62av_17 F2
 i62co_17 F2
 i62radio F2
 i63_1 F2
 i63_2 F2
 i63_3 F2
 i63_4 F2
 i63_5 F2
 i63_6 F2
 i63_7 F2
 i63_8 F2
 i63_9 F2
 i63_10 F2
 i63_11 F2
 i63_12 F2
 i63_13 F2
 i63_14 F2
 i64a F2
 i64b F2
 i65_1 F2
 i65_2 F2
 i65_3 F2
 i65_4 F2
 i65_5 F2
 i65_6 F2
 i65_7 F2
 i65_8 F2
 i65_9 F2
 i65_10 F2
 i65_11 F2
 i65_12 F2
 i65_13 F2
 i65_14 F2
 i65_15 F2
 i65_16 F2
 i65_17 F2
 i65_18 F2
 i65_19 F2
 i65_20 F2
 i65_21 F2
 i65_22 F2
 i66a F2
 i66b F2
 i66c F2
 i66d F2
 i66e F2
 i66f F2
 i66g F2
 i67 F2
 i68 F2
 i69a1 F2
 i69a2 F2
 i69b1 F2
 i69b2 F2
 i69_nomb F2
 i70 F2
 i71 F2
 i72a F2
 i72b F2
 i72c F2
 i72d_enf F2
 i72d_aut F2
 i72e F2
 i72f F2
 i72g F2
 i73 F2
 i74 F2
 i75 F2
 i76 F2
 i77 F2
 i78 F2
 i79 F2
 i80 F2
 i80_age F2
 i80b F2
 i80b_age F2
 i81 F2
 i81_age F2
 i81b F2
 i81b_age F2
 e1 F2
 e2 F2
 e3 F2
 e4a F2
 e4b F2
 e4c F2
 e5 F2.
CACHE.
EXECUTE.

* Définition des labels de variables

VARIABLE LABELS v1 "Numéro".
VARIABLE LABELS v2 "Échantillon".
VARIABLE LABELS v3 "Type de questionnaire".
VARIABLE LABELS v4 "Région".
VARIABLE LABELS q1 "Dans quelle commune habitez-vous actuellement ?".
VARIABLE LABELS q1_type "Type de commune".
VARIABLE LABELS q2 "Êtes-vous... une homme ? une femme ?".
VARIABLE LABELS q3 "En quelle année êtes-vous né(e) ?".
VARIABLE LABELS q3_age "Âge".
VARIABLE LABELS q3_const "Classes d'âge".
VARIABLE LABELS q4 "Où avez-vous passé la plus grande partie de votre enfance et de votre jeunesse (avant 16 ans) ?".
VARIABLE LABELS q4_regro "Commune d'enfance".
VARIABLE LABELS q5 "Quel est votre état civil ?".
VARIABLE LABELS q6 "En quelle année vous êtes-vous marié(e) ? [Si plusieurs, premier mariage]".
VARIABLE LABELS q6v1 "En quelle année vous êtes-vous retrouvé(e) veuf (veuve) ? [Premier veuvage]".
VARIABLE LABELS q6v2 "En quelle année vous êtes-vous retrouvé(e) veuf (veuve) ? [Second veuvage]".
VARIABLE LABELS q6d1 "En quelle année avez-vous divorcé ou vous êtes-vous séparé ? [Premier divorce]".
VARIABLE LABELS q6d2 "En quelle année avez-vous divorcé ou vous êtes-vous séparé ? [Second divorce]".
VARIABLE LABELS q6r1 "En quelle année vous êtes-vous remarié(e) ? [Premier remariage]".
VARIABLE LABELS q6r2 "En quelle année vous êtes-vous remarié(e) ? [Second remariage]".
VARIABLE LABELS q7 "Quelle est (était) l'année de naissance de votre conjoint(e) ? [Dernier mariage]".
VARIABLE LABELS q8 "Avez-vous eu des enfants ?".
VARIABLE LABELS q8_nombr "Si oui: combien d'enfants avez-vous eus ?".
VARIABLE LABELS q9_1sex "Fils / Fille [Enfant 1]".
VARIABLE LABELS q9_1nais "Année de naissance [Enfant 1]".
VARIABLE LABELS q9_1dec "Si décédé, année : [Enfant 1]".
VARIABLE LABELS q9_1domi "Domicilié à : [Enfant 1]".
VARIABLE LABELS q9_1prof "Profession [Enfant 1]".
VARIABLE LABELS q9_1pr_1 "Profession du conjoint [Enfant 1]".
VARIABLE LABELS q9_1peti "A un (ou des) enfants [Enfant 1]".
VARIABLE LABELS q9_2sex "Fils / Fille [Enfant 2]".
VARIABLE LABELS q9_2nais "Année de naissance [Enfant 2]".
VARIABLE LABELS q9_2dec "Si décédé, année : [Enfant 2]".
VARIABLE LABELS q9_2domi "Domicilié à : [Enfant 2]".
VARIABLE LABELS q9_2prof "Profession [Enfant 2]".
VARIABLE LABELS q9_2pr_1 "Profession du conjoint [Enfant 2]".
VARIABLE LABELS q9_2peti "A un (ou des) enfants [Enfant 2]".
VARIABLE LABELS q9_3sex "Fils / Fille [Enfant 3]".
VARIABLE LABELS q9_3nais "Année de naissance [Enfant 3]".
VARIABLE LABELS q9_3dec "Si décédé, année : [Enfant 3]".
VARIABLE LABELS q9_3domi "Domicilié à : [Enfant 3]".
VARIABLE LABELS q9_3prof "Profession [Enfant 3]".
VARIABLE LABELS q9_3pr_1 "Profession du conjoint [Enfant 3]".
VARIABLE LABELS q9_3peti "A un (ou des) enfants [Enfant 3]".
VARIABLE LABELS q9_4sex "Fils / Fille [Enfant 4]".
VARIABLE LABELS q9_4nais "Année de naissance [Enfant 4]".
VARIABLE LABELS q9_4dec "Si décédé, année : [Enfant 4]".
VARIABLE LABELS q9_4domi "Domicilié à : [Enfant 4]".
VARIABLE LABELS q9_4prof "Profession [Enfant 4]".
VARIABLE LABELS q9_4pr_1 "Profession du conjoint [Enfant 4]".
VARIABLE LABELS q9_4peti "A un (ou des) enfants [Enfant 4]".
VARIABLE LABELS q9_5sex "Fils / Fille [Enfant 5]".
VARIABLE LABELS q9_5nais "Année de naissance [Enfant 5]".
VARIABLE LABELS q9_5dec "Si décédé, année : [Enfant 5]".
VARIABLE LABELS q9_5domi "Domicilié à : [Enfant 5]".
VARIABLE LABELS q9_5prof "Profession [Enfant 5]".
VARIABLE LABELS q9_5pr_1 "Profession du conjoint [Enfant 5]".
VARIABLE LABELS q9_5peti "A un (ou des) enfants [Enfant 5]".
VARIABLE LABELS q9_6sex "Fils / Fille [Enfant 6]".
VARIABLE LABELS q9_6nais "Année de naissance [Enfant 6]".
VARIABLE LABELS q9_6dec "Si décédé, année : [Enfant 6]".
VARIABLE LABELS q9_6domi "Domicilié à : [Enfant 6]".
VARIABLE LABELS q9_6prof "Profession [Enfant 6]".
VARIABLE LABELS q9_6pr_1 "Profession du conjoint [Enfant 6]".
VARIABLE LABELS q9_6peti "A un (ou des) enfants [Enfant 6]".
VARIABLE LABELS q9_7sex "Fils / Fille [Enfant 7]".
VARIABLE LABELS q9_7nais "Année de naissance [Enfant 7]".
VARIABLE LABELS q9_7dec "Si décédé, année : [Enfant 7]".
VARIABLE LABELS q9_7domi "Domicilié à : [Enfant 7]".
VARIABLE LABELS q9_7prof "Profession [Enfant 7]".
VARIABLE LABELS q9_7pr_1 "Profession du conjoint [Enfant 7]".
VARIABLE LABELS q9_7peti "A un (ou des) enfants [Enfant 7]".
VARIABLE LABELS q9_8sex "Fils / Fille [Enfant 8]".
VARIABLE LABELS q9_8nais "Année de naissance [Enfant 8]".
VARIABLE LABELS q9_8dec "Si décédé, année : [Enfant 8]".
VARIABLE LABELS q9_8domi "Domicilié à : [Enfant 8]".
VARIABLE LABELS q9_8prof "Profession [Enfant 8]".
VARIABLE LABELS q9_8pr_1 "Profession du conjoint [Enfant 8]".
VARIABLE LABELS q9_8peti "A un (ou des) enfants [Enfant 8]".
VARIABLE LABELS q9_9sex "Fils / Fille [Enfant 9]".
VARIABLE LABELS q9_9nais "Année de naissance [Enfant 9]".
VARIABLE LABELS q9_9dec "Si décédé, année : [Enfant 9]".
VARIABLE LABELS q9_9domi "Domicilié à : [Enfant 9]".
VARIABLE LABELS q9_9prof "Profession [Enfant 9]".
VARIABLE LABELS q9_9pr_1 "Profession du conjoint [Enfant 9]".
VARIABLE LABELS q9_9peti "A un (ou des) enfants [Enfant 9]".
VARIABLE LABELS q10 "Avez-vous actuellement un ou des petits-enfants ou arrière-petits-enfants âgés de moins de 15 ans ?".
VARIABLE LABELS q11a "Combien aviez-vous de frères et soeurs lorsque vous aviez 20 ans environ ?".
VARIABLE LABELS q11b "Combien avez-vous de frères et soeurs vivant actuellement ?".
VARIABLE LABELS q12a "Avez-vous encore votre père ?".
VARIABLE LABELS q12b "Avez-vous encore votre mère ?".
VARIABLE LABELS q13a "Avez-vous encore votre beau-père ?".
VARIABLE LABELS q13b "Avez-vous encore votre belle-mère ?".
VARIABLE LABELS q14 "Avez-vous actuellement un ou des petits-neveux ou petites-nièces âgés de moins de 15 ans ?".
VARIABLE LABELS q15a "Faire du ménage chez eux ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15b "Préparer des repas chez eux ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15c "Leur confectionner ou leur offrir des habits ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15d "Leur faire des cadeaux assez importants ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15e "Leur faire rencontrer des personnes utiles pour leur profession ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15f "Garder leurs enfants en bas âge ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15g "Emmener leurs enfants en promenade ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) ?]".
VARIABLE LABELS q15h "Vous est-il arrivé au cours des 5 dernières années de leur prêter de l'argent (une somme d'une certainer importance) ? [Vous arrive-t-il de rendre les services suivants à l'un ou l'autre de vos enfants (ou petits-enfants ou ".
VARIABLE LABELS q16 "Si l'un ou l'autre de vos enfants (ou petits-enfants ou petits-neveux) a un commerce ou une entreprise, vous arrive-t-il de collaborer au travail de ce commerce ou de cette entreprise ?".
VARIABLE LABELS q17 "À quel âge avez-vous commencé à travailler (exercer une profession de manière régulière) ?".
VARIABLE LABELS q18a "Assurance-maladie [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances suivantes ?]".
VARIABLE LABELS q18b "Assurance-accidents (autre que celle qui va automatiquement avec l'entreprise) [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances s".
VARIABLE LABELS q18c "Assurance-vie pour vous ou votre conjoint [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances suivantes ?]".
VARIABLE LABELS q18d "Assurance responsabilité civile (autre que celle de la voiture éventuelle) [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances suiva".
VARIABLE LABELS q18e "Assurance vol et/ou incendie et/ou ménage (au moins une des trois) [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances suivantes ?]".
VARIABLE LABELS q18f "Assurance-maladie pour vos enfants [Au cours de votre vie professionnelle ou de celle de votre conjoint, est-ce que vous-même ou votre conjoint aviez conclu certaines des assurances suivantes ?]".
VARIABLE LABELS q19a "Au cours de votre vie professionnelle ou de celle de votre conjoint, avez-vous cotisé (ou votre conjoint a-t-il cotisé) à une caisse de retraite (2ème pilier) ?".
VARIABLE LABELS q19b "À partir de quelle année environ ?".
VARIABLE LABELS q20 "Quel est votre grade militaire ?".
VARIABLE LABELS q21 "Entre le moment où vous aviez 45 ans et maintenant, est-ce que vous avez dû, à un moment ou à un autre, renoncer entièrement ou en partie à exercer votre travail ou vos activités quotidiennes habituelles durant deux mois au moin".
VARIABLE LABELS q22a "Pouvez-vous situer cette interruption (ou ces interruptions) à un ou deux ans près ? Quel âge aviez-vous environ au début de la première interruption ?".
VARIABLE LABELS q22b "S'il y en a eu d'autres, [quel âge aviez-vous environ au début de] la deuxième interruption ?".
VARIABLE LABELS q22c "S'il y en a eu d'autres, [quel âge aviez-vous environ au début de] la troisième interruption ?".
VARIABLE LABELS q22d "S'il y en a eu d'autres, [quel âge aviez-vous environ au début de] la quatrième interruption ?".
VARIABLE LABELS q22e "Si cela vous est arrivé plus de 4 fois, quel âge aviez-vous au début de la dernière ?".
VARIABLE LABELS q23a "S'agissait-il d'un accident ou d'une maladie ? [(Première) interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q23b "Après convalescence ou rétablissement avez-vous pu continuer à exercer la même activité et à remplir les mêmes tâches courantes qu'auparavant ? [(Première) interruption de vos activités courantes pour des raisons de santé après".
VARIABLE LABELS q23c "Êtes-vous aujourd'hui parfaitement remis(e) ou non de cette maladie (ou accident) et de ses suites ? [(Première) interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q23d "Cette interruption a-t-elle provoqué un déséquilibre de votre budget dans les mois, voire les années qui ont suivi ? [(Première) interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q24a "S'agissait-il d'un accident ou d'une maladie ? [Dernière interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q24b "Après convalescence ou rétablissement avez-vous pu continuer à exercer la même activité et à remplir les mêmes tâches courantes qu'auparavant ? [Dernière interruption de vos activités courantes pour des raisons de santé après 4".
VARIABLE LABELS q24c "Êtes-vous aujourd'hui parfaitement remis(e) ou non de cette maladie (ou accident) et de ses suites ? [Dernière interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q24d "Cette interruption a-t-elle provoqué un déséquilibre de votre budget dans les mois, voire les années qui ont suivi ? [Dernière interruption de vos activités courantes pour des raisons de santé après 45 ans]".
VARIABLE LABELS q25a "J'ai bon appétit".
VARIABLE LABELS q25b "Je me sens un peu isolé(e), un peu seul(e), même parmi des amis".
VARIABLE LABELS q25c "J'ai de la peine à dormir la nuit".
VARIABLE LABELS q25d "Je prends plaisir à ce que je fais".
VARIABLE LABELS q25e "Je me sens triste".
VARIABLE LABELS q25f "J'ai confiance en moi".
VARIABLE LABELS q25g "J'ai des crises de larmes et envie de pleurer".
VARIABLE LABELS q25h "Je me sens fatigué(e)".
VARIABLE LABELS q25i "Je me sens irritable".
VARIABLE LABELS q25j "J'ai confiance en l'avenir".
VARIABLE LABELS q25k "Je me sens anxieux (anxieuse), angoissé(e)".
VARIABLE LABELS q26a "Dans la vie de tous les jours arrive-t-il que votre mémoire vous joue des tours ?".
VARIABLE LABELS q26b "Vous arrive-t-il de passer plusieurs heures, voire une journée entière, sans pouvoir vous occuper de rien, parce que nous n'arrivez pas à vous y mettre ?".
VARIABLE LABELS q27a "Pouvez-vous vous déplacer d'une pièce à l'autre ?".
VARIABLE LABELS q27b "Pouvez-vous monter et descendre un escalier ?".
VARIABLE LABELS q27c "Pouvez-vous vous déplacer à l'extérieur de votre maison ?".
VARIABLE LABELS q27d "Pouvez-vous parcourir 400 m. à pied ?".
VARIABLE LABELS q27e "Pouvez-vous vou coucher et vous lever seul(e) ?".
VARIABLE LABELS q27f "Pouvez-vous vous habiller et vous déshabiller seul(e) ?".
VARIABLE LABELS q27g "Pouvez-vous vous laver entièrement ?".
VARIABLE LABELS q27h "Pouvez-vous vous couper les ongles des pieds ?".
VARIABLE LABELS q27i "Pouvez-vous porter un objet lourd (5 kg) ?".
VARIABLE LABELS q27j "Pouvez-vous couper vous-mêmes vos aliments (viande ou fruits par exemple) ?".
VARIABLE LABELS q27k "Est-ce que votre vue est suffisamment bonne pour vous permettre de lire un texte normalement imprimé dans un journal (avec vos lunettes si vous en avez) ?".
VARIABLE LABELS q27l "Pouvez-vous entendre ce que dit une autre personne au cours d'une conversation normale (avec votre appareil acoustique si vous en avez un) ?".
VARIABLE LABELS q27m "Pouvez-vous suivre une conversation à laquelle participent plusieurs personnes (avec votre appareil acoustique si vous en avez un) ?".
VARIABLE LABELS q28 "Êtes-vous actuellement inscrit(e) à une (ou plusieurs) associaton(s) ?".
VARIABLE LABELS q28_mens "À combien de réunions participez-vous par mois, en moyenne (en temps normal, sans tenir compte des éventuelles périodes de vacances ou de maladie par exemple) ?".
VARIABLE LABELS q29a "Association sportive [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29b "Association de loisirs [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29c "Association politique ou syndicale [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29d "Association patriotique [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29e "Association professionnelle [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29f "Association religieuse ou paroissiale (y compris le chant sacré) [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29g "Association musicale (fanfare, chorale, etc.) [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29h "Association de bienfaisance [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29i "Association de quartier [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q29j "Autre [Participez-vous au moins une fois par an à la vie d'une association de l'un des types suivants ?]".
VARIABLE LABELS q30 "Exercez-vous certaines responsabilités (président(e), membre du comité, secrétaire, caissier (caissière)) dans l'une ou l'autre de ces associations ?".
VARIABLE LABELS q31 "Certaines associations à la vie desquelles vous participez (l'association à la vie de laquelle vous participez) ont-elles été créées spécialement pour les personnes âgées ?".
VARIABLE LABELS q32 "Possédez-vous (ou votre conjoint possède-t-il) une voiture ?".
VARIABLE LABELS q33 "À quelle fréquence utilisez-vous votre voiture, en temps normal ?".
VARIABLE LABELS q34 "En avez-vous possédé une ?".
VARIABLE LABELS q34a "En quelle année environ l'avez-vous abandonnée ?".
VARIABLE LABELS q34b "Pour quelle raison principale avez-vous renoncé ?".
VARIABLE LABELS q35 "Depuis combien de temps environ habitez-vous votre logement actuel ?".
VARIABLE LABELS q36 "Depuis combien de temps environ habitez-vous dans la commune (en ville : dans le quartier) où vous habitez actuellement ?".
VARIABLE LABELS q37a "Commune (en ville : quartier) [Si votre dernier déménagement a eu lieu il y a moins de 15 ans, où habitiez-vous avant de déménager ?]".
VARIABLE LABELS q37b "Canton (ou pays) [Si votre dernier déménagement a eu lieu il y a moins de 15 ans, où habitiez-vous avant de déménager ?]".
VARIABLE LABELS q38a "Raisons liées à votre travail ou à la retraite [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38b "Vous vouliez vous rapprocher de vos enfants [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38c "Vous vouliez vous rapprocher de parents ou d'amis [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38d "Des raisons de santé [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38e "Des raisons financières [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38f "Une expropriation, un bail non renouvelable [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q38g "Des raisons de commodité matérielle (appartement trop grand, peu confortable, trop éloigné des équipements collectifs) [Parmi les raisons énumérées ci-dessous, pouvez-vous cocher celles qui ont motivé ce déménagement ?]".
VARIABLE LABELS q39a "Le plus grand [Si vous comparez votre ancien et votre nouveau domicile, pouvez-vous nous dire lequel des deux était (est) :]".
VARIABLE LABELS q39b "Le mieux équipé [Si vous comparez votre ancien et votre nouveau domicile, pouvez-vous nous dire lequel des deux était (est) :]".
VARIABLE LABELS q39c "Le moins bruyant [Si vous comparez votre ancien et votre nouveau domicile, pouvez-vous nous dire lequel des deux était (est) :]".
VARIABLE LABELS q39d "Le mieux adapté à des personnes de votre âge actuel [Si vous comparez votre ancien et votre nouveau domicile, pouvez-vous nous dire lequel des deux était (est) :]".
VARIABLE LABELS q39e "Dans lequel les contacts avec les voisins sont-il les meilleurs [Si vous comparez votre ancien et votre nouveau domicile, pouvez-vous nous dire lequel des deux était (est) :]".
VARIABLE LABELS q40a "À l'épicerie ou au magasin d'alimentation où vous vous servez habituellement [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement :]".
VARIABLE LABELS q40b "À l'arrêt de bus (tram), ou de car postal le plus proche [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement :]".
VARIABLE LABELS q40c "Au café ou au restaurant que vous fréquentez le plus souvent [Combien de temps vous faut-il pour vous rendre à pied, à votre allure habituelle, de votre logement :]".
VARIABLE LABELS ie1 "Ce questionnaire [auto-administré] était-il :".
VARIABLE LABELS ie3 "La personne à interroger habite-t-elle :".
VARIABLE LABELS i1_age "Âge de l'interviewé(e)".
VARIABLE LABELS i2 "Êtes-vous propriétaire, locataire ou sous-locataire du logement que vous habitez ?".
VARIABLE LABELS i3 "Vivez-vous seul(e) ?".
VARIABLE LABELS i4 "Souhaiteriez-vous vivre avec quelqu'un d'autre ?".
VARIABLE LABELS i4a "Enfant(s) [Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour personnes âgées ou avec d'autres personnes encore ?]".
VARIABLE LABELS i4b "Parent(s) [Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour personnes âgées ou avec d'autres personnes encore ?]".
VARIABLE LABELS i4c "Ami(s) [Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour personnes âgées ou avec d'autres personnes encore ?]".
VARIABLE LABELS i4d "Pension, institution [Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour personnes âgées ou avec d'autres personnes encore ?]".
VARIABLE LABELS i4e "Autre [Avec qui souhaiteriez-vous vivre ? Est-ce avec vos enfants, des parents, des amis, dans une pension ou une institution pour personnes âgées ou avec d'autres personnes encore ?]".
VARIABLE LABELS i5a "Combien de personnes vivent avec vous ? (Dans le même appartement, vous non compris)".
VARIABLE LABELS i5ba "Conjoint [Quelles sont ces personnes ?]".
VARIABLE LABELS i5bb "Ami(e) [Quelles sont ces personnes ?]".
VARIABLE LABELS i5bc "Enfant(s) [Quelles sont ces personnes ?]".
VARIABLE LABELS i5bd "Autre parent [Quelles sont ces personnes ?]".
VARIABLE LABELS i5be "Locataire [Quelles sont ces personnes ?]".
VARIABLE LABELS i5bf "Domestique, bonne [Quelles sont ces personnes ?]".
VARIABLE LABELS i5bg "Autre [Quelles sont ces personnes ?]".
VARIABLE LABELS i6a "Habitez-vous chez vous ou chez eux (chez lui, chez elle) ?".
VARIABLE LABELS i6b "Depuis combien de temps environ habitez-vous avec eux (lui, elle) ?".
VARIABLE LABELS i7 "Combien de pièces y a-t-il dans votre appartement (cuisine, et salle de bains non comprises) ?".
VARIABLE LABELS i8 "Quelle profession exerçait votre père ?".
VARIABLE LABELS i9 "Quelle est la dernière école que vous avez fréquentée régulièrement ?".
VARIABLE LABELS i10_annee "Passons maintenant à quelques questions sur votre situation à 45 ans. Cela se passait en :".
VARIABLE LABELS i10_comm "Commune [À cette époque, quand vous aviez 45 ans environ, dans quelle commune résidiez-vous ?]".
VARIABLE LABELS i10_cant "Canton ou pays [À cette époque, quand vous aviez 45 ans environ, dans quelle commune résidiez-vous ?]".
VARIABLE LABELS i11 "À 45 ans environ, exerciez-vous une activité professionnelle à plein temps ou (en dehors des éventuelles périodes sous les drapeaux) à temps partiel ?".
VARIABLE LABELS i12a "Avant votre (1er) mariage [Avez-vous exercé une activité professionnelle régulière, à mi-temps ou plus, durant les périodes suivantes de votre vie :]".
VARIABLE LABELS i12b "Lorsque vous aviez des enfants en bas âge (moins de 10 ans) [Avez-vous exercé une activité professionnelle régulière, à mi-temps ou plus, durant les périodes suivantes de votre vie :]".
VARIABLE LABELS i12c "Lorsque vos enfants avaient tous 10 ans au moins [Avez-vous exercé une activité professionnelle régulière, à mi-temps ou plus, durant les périodes suivantes de votre vie :]".
VARIABLE LABELS i12d "Lorsque vos enfants ont quitté la maison [Avez-vous exercé une activité professionnelle régulière, à mi-temps ou plus, durant les périodes suivantes de votre vie :]".
VARIABLE LABELS i13 "Avez-vous repris une activité professionnelle régulière, à mi-temps au moins, entre 45 et 55 ans ?".
VARIABLE LABELS i14a "Quelle était alors votre profession exacte ? (profession principale)".
VARIABLE LABELS i14b "Profession indépendante ou non ?".
VARIABLE LABELS i14c "Aviez-vous des subordonnés et si oui combien environ ?".
VARIABLE LABELS i14d "Travail rémunéré :".
VARIABLE LABELS i14e "Aviez-vous alors une profession accessoire dans l'agriculture, la viticulture, le tourisme, le commerce, par exemple ?".
VARIABLE LABELS i14f "Préciser également si cette profession était exercée :".
VARIABLE LABELS i15a "Association sportive [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15b "Association de loisirs [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15c "Association politique ou syndicale [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15d "Association patriotique [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15e "Association professionnelle [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15f "Association religieuse ou paroissiale (y compris chant sacré) [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15g "Association musicale (fanfares, chorales) [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15h "Association de bienfaisance [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15i "Association libre, groupement de quartier [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15j "Autre (a) [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i15k "Autre (b) [Lorsque vous aviez 45 ans environ participiez-vous, au moins une fois par an, à des associations, groupements, fédérations, clubs, etc., des types suivants :]".
VARIABLE LABELS i16 "Exerciez-vous certaines responsabilités (président, membre du comité, secrétaire, caissier) dans l'une ou l'autre de ces associations ?".
VARIABLE LABELS i17a "Comment évaluez-vous votre état de santé lorsque vous aviez 40 à 45 ans ? Diriez-vous qu'il était :".
VARIABLE LABELS i17b "Quel genre de troubles, durables ou chroniques aviez-vous alors ?".
VARIABLE LABELS i18a "Effectué des visites régulières (tous les 1 ou 2 ans) chez le médecin pour faire contrôler votre santé (donc en dehors des maladies déclarées) [Lorsque vous aviez 40 à 45 ans environ, est-ce que vous avez :]".
VARIABLE LABELS i18b "Effectué des visites régulières (tous les ans environ) de contrôle chez le dentiste [Lorsque vous aviez 40 à 45 ans environ, est-ce que vous avez :]".
VARIABLE LABELS i18c "Est-ce que vous fumiez des cigarettes, des cigares ou la pipe ? [Lorsque vous aviez 40 à 45 ans environ, est-ce que vous avez :]".
VARIABLE LABELS i18d "Estimez-vous qu'actuellement vous fumez plus, la même chose ou moins qu'à cet âge, ou avez-vous arrêté ou dû arrêter ?".
VARIABLE LABELS i19 "Entre l'âge de 45 ans environ (celui dont nous avons parlé il y a un instant) et l'âge de 60 ans, avez-vous cessé d'exercer votre activité professionnelle (ou pris votre retraite) ?".
VARIABLE LABELS i19a "De profession [Avez-vous changé durant cette période :]".
VARIABLE LABELS i19b "D'entreprise [Avez-vous changé durant cette période :]".
VARIABLE LABELS i19c "De place dans la hiérarchie de l'entreprise [Avez-vous changé durant cette période :]".
VARIABLE LABELS i19d "De mode de rétribution [Avez-vous changé durant cette période :]".
VARIABLE LABELS i20 "Pouvez-vous me décrire votre profession à 60 ans ? [Code profession]".
VARIABLE LABELS i20a "Profession indépendante ou non ?".
VARIABLE LABELS i20b "Aviez-vous des subordonnés et si oui, combien environ ?".
VARIABLE LABELS i20c "Travail rémunéré :".
VARIABLE LABELS i21a "De la variété des tâches à accomplir [Comment jugez-vous le travail que vous faisiez à 60 ans environ ? Le trouviez-vous très intéressant, intéressant, plus ou moins intéressant, peu intéressant ou pas du tout intéressant du po".
VARIABLE LABELS i21b "Des responsabilités à assumer [Comment jugez-vous le travail que vous faisiez à 60 ans environ ? Le trouviez-vous très intéressant, intéressant, plus ou moins intéressant, peu intéressant ou pas du tout intéressant du point de ".
VARIABLE LABELS i21c "Des contacts avec vos collègues de travail [Comment jugez-vous le travail que vous faisiez à 60 ans environ ? Le trouviez-vous très intéressant, intéressant, plus ou moins intéressant, peu intéressant ou pas du tout intéressant".
VARIABLE LABELS i21d "Du salaire (de sa rémunération) [Comment jugez-vous le travail que vous faisiez à 60 ans environ ? Le trouviez-vous très intéressant, intéressant, plus ou moins intéressant, peu intéressant ou pas du tout intéressant du point d".
VARIABLE LABELS i21e "De l'expérience qu'il demandait [Comment jugez-vous le travail que vous faisiez à 60 ans environ ? Le trouviez-vous très intéressant, intéressant, plus ou moins intéressant, peu intéressant ou pas du tout intéressant du point d".
VARIABLE LABELS i21f "Tout compte fait comment était-il en général ?".
VARIABLE LABELS i22a "Le travail que vous faisiez alors était-il pénible physiquement, demandait-il de gros efforts ?".
VARIABLE LABELS i22b "Les conditions de travail laissaient-elles à désirer du point de vue du bruit, de la poussière, de la lumière, des odeurs, de la chaleur (en général) ?".
VARIABLE LABELS i23a "Votre situation professionnelle s'est-elle améliorée, est-elle restée la même ou s'est-elle détériorée ? [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses peuvent changer dans l'activité professionnelle. (.".
VARIABLE LABELS i23b "Votre travail est-il devenu plus varié et plus intéressant, est-il resté le même ou est-il devenu plus monotone ? [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses peuvent changer dans l'activité professionn".
VARIABLE LABELS i23c "Votre travail est-il devenu physiquement plus pénible, est-il resté le même ou est-il devenu moins pénible ? [Entre l'âge de 45 ans et l'âge de 60 ans environ, beaucoup de choses peuvent changer dans l'activité professionnelle.".
VARIABLE LABELS i23d "Est-ce que les conditions de l'emploi, mis à part le salaire, comme par ex. la sécurité de l'emploi, le nombre de jours de congé, la durée des vacances, etc., se sont améliorées, ou sont-elles à peu près les mêmes, ou se sont-e".
VARIABLE LABELS i24 "Avez-vous pris votre retraite de la profession que vous exerciez à 60 ans environ ?".
VARIABLE LABELS i24_age "Si oui : à quel âge ?".
VARIABLE LABELS i25 "Avez-vous pris votre retraite de l'activité accessoire dont vous avez parlé ?".
VARIABLE LABELS i25_age "Si oui : à quel âge ?".
VARIABLE LABELS i26 "Revenons à votre retraite de votre profession principale : un âge légal de retraite était-il alors fixe dans votre profession ?".
VARIABLE LABELS i26b "Si oui, avez-vous pris votre retraite à cet âge, ou s'agissait-il d'une retraite anticipée ou repoussée ?".
VARIABLE LABELS i27a "Des problèmes de l'entreprise [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite :]".
VARIABLE LABELS i27b "Vous aviez des problèmes de santé [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite :]".
VARIABLE LABELS i27c "Vous en aviez assez de votre travail [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite :]".
VARIABLE LABELS i27d "Du point de vue financier, vous aviez une retraite suffisante pour pouvoir arrêter de travailler [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite :]".
VARIABLE LABELS i27e "Autres raisons [Les raisons suivantes sont-elles intervenues dans votre passage à la retraite :]".
VARIABLE LABELS i28a "Nécessités financières [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail ? S'agissait-il de :]".
VARIABLE LABELS i28b "Intérêt professionnel [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail ? S'agissait-il de :]".
VARIABLE LABELS i28c "Besoin de contact avec des collègues de travail [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail ? S'agissait-il de :]".
VARIABLE LABELS i28d "Désir d'une vie bien réglée [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail ? S'agissait-il de :]".
VARIABLE LABELS i28e "Autres raisons [Quelles sont les raisons qui vous ont poussé à poursuivre votre travail ? S'agissait-il de :]".
VARIABLE LABELS i29a "Voyez-vous encore certains de vos anciens collègues de travail ?".
VARIABLE LABELS i29b "Où les rencontrez-vous principalement ?".
VARIABLE LABELS i30a "La compagnie ou les contacts humains [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i30b "Le sentiment d'être utile [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i30c "La considération, pouvoir dire son mot [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i30d "L'intérêt que présentait votre travail [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i30e "Les rentrées d'argent [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i30f "Autre [Les choses suivantes vous manquent-elles beaucoup, un peu ou pas du tout depuis votre retraite ?]".
VARIABLE LABELS i31a "La vie paisible [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i31b "Ne plus être lié, ne plus avoir de responsabilités [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i31c "L'occupation personnelle [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i31d "Ne plus devoir travailler [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i31e "La pension [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i31f "Autre [Dans la vie de retraité, diriez-vous que vous appréciez beaucoup, un peu ou pas du tout les choses suivantes :]".
VARIABLE LABELS i32 "Avez-vous gardé une activité rémunérée ou en avez-vous repris une nouvelle ?".
VARIABLE LABELS i33 "S'agit-il de : [Si oui]".
VARIABLE LABELS i34_prin "Quelles sont les raisons qui vous poussent à poursuivre ce travail ? (2 réponses possibles: raison principale) [Si non]".
VARIABLE LABELS i34_seco "Quelles sont les raisons qui vous poussent à poursuivre ce travail ? (2 réponses possibles: 2ème raison) [Si non]".
VARIABLE LABELS i35 "Avez-vous déjà décidé de (re)prendre votre retraite dans les années à venir, ou souhaitez-vous continuer votre activité le plus longtemps possible ?".
VARIABLE LABELS i36 "Quel est votre état civil ? (actuel)".
VARIABLE LABELS i37a "Les problèmes financiers [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37b "Le manque de contact sociaux [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37c "La solitude [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37d "La difficulté à trouver du travail [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37e "S'habituer à tout faire seul(e) [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37f "Prendre seul(e) certaines responsabilités [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37g "La vie n'avait plus de sens [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i37h "Autre [Lorsque vous vous êtes retrouvé(e) seul(e) après le décès de votre conjoint, les problèmes que je vais énumérer mainetnant ont-ils été pour vous...]".
VARIABLE LABELS i38 "Pour faire face à ces problèmes, avez-vous pu compter sur l'aide de quelqu'un ?".
VARIABLE LABELS i38a "De vos enfants [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38b "De vos parents [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38c "De vos beaux-parents [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38d "De vos frères et soeurs [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38e "D'amis [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38f "De services sociaux ou d'associations religieuses [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38g "Autres aides [Si oui, avez-vous pu compter sur l'aide:]".
VARIABLE LABELS i38h "Si non, en avez-vous cherché une ?".
VARIABLE LABELS i39 "Quelle profession exerçait votre conjoint lorsqu'il/elle avait 40 à 45 ans environ ?".
VARIABLE LABELS i39_anne "Avait arrêté en 19xx environ".
VARIABLE LABELS i39_repr "A repris par la suite".
VARIABLE LABELS i39_prof "Profession du conjoint lorsqu'il avait 40 à 45 ans".
VARIABLE LABELS i39_pr_1 "Profession indépendante ou non ?".
VARIABLE LABELS i39_pr_2 "Avait-il/elle des subordonnés et si oui, combien environ ?".
VARIABLE LABELS i39_pr_3 "Travail rémunéré :".
VARIABLE LABELS i40 "En quelle année votre mari a-t-il pris sa retraite ?".
VARIABLE LABELS i41 "S'agissait-il, par rapport à l'âge fixé dans sa profession, ou (s'il n'y en avait pas) par rapport à l'âge AVS :".
VARIABLE LABELS i43 "Actuellement, cela [ce qui a été le plus gênant] est-il ?".
VARIABLE LABELS i44a "Après sa retraite, votre mari a-t-il repris une activité professionnelle ?".
VARIABLE LABELS i44b "L'exerce-t-il encore toujours ?".
VARIABLE LABELS i45 "Comment évaluez-vous votre état de santé ? Diriez-vous qu'il est :".
VARIABLE LABELS i46 "Si vous comparez votre état de santé à celui des personnes qui ont le même âge que vous, le vôtre est-il :".
VARIABLE LABELS i47a1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Douleurs, crampes, tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47a2 "S'agit-il de troubles... [Douleurs, crampes tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47a3 "S'agit-il de troubles... [Douleurs, crampes tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47a4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Douleurs, crampes tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47a5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Douleurs, crampes tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47a6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Douleurs, crampes tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47b1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47b2 "S'agit-il de troubles... [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47b3 "S'agit-il de troubles... [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47b4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47b5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47b6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Douleurs, crampes, tremblements ou enflure des membres supérieurs (mains, poignets, coudes, bras, épaules)]".
VARIABLE LABELS i47c1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47c2 "S'agit-il de troubles... [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47c3 "S'agit-il de troubles... [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47c4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47c5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47c6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Maux de tête, douleurs de la nuque, du dos, des reins]".
VARIABLE LABELS i47d1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47d2 "S'agit-il de troubles... [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47d3 "S'agit-il de troubles... [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47d4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47d5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47d6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Troubles de l'ouïe, même malgré le port d'un appareil acoustique]".
VARIABLE LABELS i47e1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47e2 "S'agit-il de troubles... [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47e3 "S'agit-il de troubles... [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47e4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47e5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47e6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Troubles de la vue, même malgré le port de lunettes]".
VARIABLE LABELS i47f1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47f2 "S'agit-il de troubles... [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47f3 "S'agit-il de troubles... [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47f4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47f5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47f6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Troubles de la voix ou de l'élucution]".
VARIABLE LABELS i47g1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47g2 "S'agit-il de troubles... [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47g3 "S'agit-il de troubles... [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47g4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47g5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47g6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Palpitations ou douleurs au niveau du coeur]".
VARIABLE LABELS i47h1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47h2 "S'agit-il de troubles... [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47h3 "S'agit-il de troubles... [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47h4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47h5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47h6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Difficultés respiratoires, asthme, toux chroniques, essouflement]".
VARIABLE LABELS i47i1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47i2 "S'agit-il de troubles... [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47i3 "S'agit-il de troubles... [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47i4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47i5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47i6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Maux de gorge, maux d'estomac, difficulté de digestion, constipation, diarrhée]".
VARIABLE LABELS i47j1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47j2 "S'agit-il de troubles... [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47j3 "S'agit-il de troubles... [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47j4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47j5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47j6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Douleurs, gêne ou mauvais fonctionnement des organes génitaux ou urinaires]".
VARIABLE LABELS i47k1 "Concernant votre santé, souffrez-vous actuellement des troubles suivants ? [Douleurs, crampes, tremblements ou enflure des membres inférieurs (pieds, jambes, genou, hanches...)]".
VARIABLE LABELS i47k2 "S'agit-il de troubles... [Autres troubles : gêne, malaises, démangeaisons]".
VARIABLE LABELS i47k3 "S'agit-il de troubles... [Autres troubles : gêne, malaises, démangeaisons]".
VARIABLE LABELS i47k4 "Ces troubles vous gênent-ils dans vos activités journalières habituelles ? [Autres troubles : gêne, malaises, démangeaisons]".
VARIABLE LABELS i47k5 "Consultez-vous régulièrement un médecin au sujet de ces troubles ? [Autres troubles : gêne, malaises, démangeaisons]".
VARIABLE LABELS i47k6 "Un traitement médical vous a-t-il été prescrit pour ces troubles ? [Autres troubles : gêne, malaises, démangeaisons]".
VARIABLE LABELS i51f "Autre personnel de santé [Depuis une année environ, avez-vous été suivi par :]".
VARIABLE LABELS i52a "Votre mari/femme [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i52b "Vos enfants [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i52c "Des parents (parenté) [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i52d "D'amis [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i52e "De voisins [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i52f "Autres pesronnes [Supposons que vous soyez malade, qui s'occupera de vous ? Pouvez-vous ou pourriez-vous compter sur une aide, partielle ou complète de :]".
VARIABLE LABELS i53 "Y a-t-il dans votre famille, votre entourage ou votre voisinage une personne nécessitant des soins pour des raisons de santé et dont vous vous occupez régulièrement ?".
VARIABLE LABELS i53_type "Si oui, qui :".
VARIABLE LABELS i54a "Voisins (bénévoles) [Pour accomplir les tâches de votre ménage ou de la vie courante est-ce qu'il vous arrive de bénéficier de l'aide de :]".
VARIABLE LABELS i54b "Membre(s) de votre famille n'habitant pas avec vous [Pour accomplir les tâches de votre ménage ou de la vie courante est-ce qu'il vous arrive de bénéficier de l'aide de :]".
VARIABLE LABELS i54c "D'une personne d'un service social ou d'un centre médico-social [Pour accomplir les tâches de votre ménage ou de la vie courante est-ce qu'il vous arrive de bénéficier de l'aide de :]".
VARIABLE LABELS i54d "D'une autre personne rémunérée par vous [Pour accomplir les tâches de votre ménage ou de la vie courante est-ce qu'il vous arrive de bénéficier de l'aide de :]".
VARIABLE LABELS i54e "Autre [Pour accomplir les tâches de votre ménage ou de la vie courante est-ce qu'il vous arrive de bénéficier de l'aide de :]".
VARIABLE LABELS i55a "Préparer les repas principaux [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55b "Faire les achats courants [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55c "Faire les grands nettoyages [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55d "Faire les lits [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55e "Faire la vaisselle [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55f "Faire la lessive [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i55g "Remplir la feuille d'impôts [Dans votre ménage accomplissez-vous toujours ou presque les tâches suivantes ou les partagez-vous avec quelqu'un d'autre ou sont-elles toujours ou presque accomplies par quelqu'un d'autre (...)]".
VARIABLE LABELS i56a "Eau chaude à volonté [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56b "W.-C. dans l'appartement [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56c "Salle de bains dans l'appartement [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56d "Un frigo [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56e "Chauffage central [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56f "Le téléphone [Avez-vous dans votre appartement :]".
VARIABLE LABELS i56g "Si vous devez téléphoner d'urgence, pouvez-vous le faire :".
VARIABLE LABELS i57a "Avez-vous à votre disposition un jardin ?".
VARIABLE LABELS i57b "Possédez-vous un tourne-disque ou un enregistreur ?".
VARIABLE LABELS i57c "Possédez-vous la radio ?".
VARIABLE LABELS i57d "Possédez-vous la télévision ?".
VARIABLE LABELS i57e "Allez-vous la regarder assez régulièrement (au moins une fois par semaine) chez des parents, des voisins ou au café ?".
VARIABLE LABELS i58 "À quelle fréquence écoutez-vous la radio ?".
VARIABLE LABELS i59 "À quelle fréquence regardez-vous la télévision et, si vous la regardez presque tous les jours, combien d'heures par jour en moyenne ?".
VARIABLE LABELS i60 "Lisez-vous régulièrement (c'est-à-dire tous les jours ou presque) un ou plusieurs quotidiens ?".
VARIABLE LABELS i61 "En temps normal, combien de temps consacrez-vous chaque jour à la lecture de livres, de journau, Xde revues ? (temps total de lecture)".
VARIABLE LABELS i62ac_01 "Promenades à pied (en dehors des courses dans les magasins) [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_01 "Promenades à pied (en dehors des courses dans les magasins) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_01 "Promenades à pied (en dehors des courses dans les magasins) [Comparaison]".
VARIABLE LABELS i62ac_02 "Aller au café, au restaurant, au tea-room [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_02 "Aller au café, au restaurant, au tea-room [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_02 "Aller au café, au restaurant, au tea-room [Comparaison]".
VARIABLE LABELS i62ac_03 "Aller au concert, au théâtre, au cinéma [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_03 "Aller au concert, au théâtre, au cinéma [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_03 "Aller au concert, au théâtre, au cinéma [Comparaison]".
VARIABLE LABELS i62ac_04 "Déplacements de 3-4 jours au moins (chez des parents, des amis ou en vacances) [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréq".
VARIABLE LABELS i62av_04 "Déplacements de 3-4 jours au moins (chez des parents, des amis ou en vacances) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_04 "Déplacements de 3-4 jours au moins (chez des parents, des amis ou en vacances) [Comparaison]".
VARIABLE LABELS i62ac_05 "Exercices physiques réguliers, gymnastique [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_05 "Exercices physiques réguliers, gymnastique [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_05 "Exercices physiques réguliers, gymnastique [Comparaison]".
VARIABLE LABELS i62ac_06 "Pratiquer un sport [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_06 "Pratiquer un sport [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_06 "Pratiquer un sport [Comparaison]".
VARIABLE LABELS i62ac_07 "Ecouter des concerts (à la radio, disques, cassettes) [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_07 "Ecouter des concerts (à la radio, disques, cassettes) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_07 "Ecouter des concerts (à la radio, disques, cassettes) [Comparaison]".
VARIABLE LABELS i62ac_08 "Jouer d'un instrument [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_08 "Jouer d'un instrument [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_08 "Jouer d'un instrument [Comparaison]".
VARIABLE LABELS i62ac_09 "Jeux de société (cartes, échecs, monopoly, loto...) [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_09 "Jeux de société (cartes, échecs, monopoly, loto...) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_09 "Jeux de société (cartes, échecs, monopoly, loto...) [Comparaison]".
VARIABLE LABELS i62ac_10 "Faire des mots-croisés [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_10 "Faire des mots-croisés [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_10 "Faire des mots-croisés [Comparaison]".
VARIABLE LABELS i62ac_11 "Jardinage [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_11 "Jardinage [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_11 "Jardinage [Comparaison]".
VARIABLE LABELS i62ac_12 "Bricolage, réparations (mécanique, menuiserie, ou des travaux manuels: tricot, couture, poterie...) [Je vais énumérer maintenant une liste d'activités. (...)]".
VARIABLE LABELS i62av_12 "Bricolage, réparations (mécanique, menuiserie, ou des travaux manuels: tricot, couture, poterie...) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_12 "Bricolage, réparations (mécanique, menuiserie, ou des travaux manuels: tricot, couture, poterie...) [Comparaison]".
VARIABLE LABELS i62ac_13 "Avoir un animal domestique dont on s'occupe [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_13 "Avoir un animal domestique dont on s'occupe [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_13 "Avoir un animal domestique dont on s'occupe [Comparaison]".
VARIABLE LABELS i62ac_14 "Assister à diverses manifestations politiques ou syndicales [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_14 "Assister à diverses manifestations politiques ou syndicales [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_14 "Assister à diverses manifestations politiques ou syndicales [Comparaison]".
VARIABLE LABELS i62ac_15 "Assister à diverses manifestations villageoises ou de quartier (fête de village par exemple) [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et".
VARIABLE LABELS i62av_15 "Assister à diverses manifestations villageoises ou de quartier (fête de village par exemple) [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_15 "Assister à diverses manifestations villageoises ou de quartier (fête de village par exemple) [Comparaison]".
VARIABLE LABELS i62ac_16 "Prier [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_16 "Prier [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_16 "Prier [Comparaison]".
VARIABLE LABELS i62ac_17 "Aller à l'église ou au temple [Je vais énumérer maintenant une liste d'activités. Pouvez-vous me dire si vous les avez pratiquées au cours de l'année écoulée et à quelle fréquence.]".
VARIABLE LABELS i62av_17 "Aller à l'église ou au temple [À 45 ans. Pratiquait ?]".
VARIABLE LABELS i62co_17 "Aller à l'église ou au temple [Comparaison]".
VARIABLE LABELS i62radio "S'il n'y va plus actuellement, mais suit le culte à la radio ou à la TV, cocher ici et indiquer la fréquence".
VARIABLE LABELS i63_1 "Pratiquer une activité artistique telle que dessin, peinture, photo, ou étude d'une langue ou d'une science [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_2 "Suivre à l'extérieur des cours ou des conférences de divers genres [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_3 "Promenades d'un jour ou deux en voiture, en car, ou en train [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_4 "Recevoir chez vous des membres de votre famille (ne vivant pas avec vous) [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_5 "Recevoir chez vous des amis ou des connaissances [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_6 "Être invité chez des membres de votre famille [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_7 "Être invité chez des amis ou des connaissances [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_8 "Téléphoner à des membres de votre famille, des amis ou des connaissances [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_9 "Recevoir des téléphones de la part de ces mêmes personnes [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_10 "Écrire des lettres [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_11 "Recevoir vos petits-enfants ou des parents en vacances [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_12 "Garder des enfants à la journée ou le soir (enfants n'habitant pas avec vous) [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_13 "Autres activités bénévoles [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i63_14 "Aller voter [Voici encore quelques autres activités, j'aimerais savoir si vous les pratiquez actuellement.]".
VARIABLE LABELS i64a "Que vous recevez plus souvent ou moins souvent qu'alors des parents, des amis ou des connaissances chez vous ? [D'une manière générale, et si vous comparez la situation actuelle à ce qui se passait lorsque vous aviez 40 à 50 an".
VARIABLE LABELS i64b "Que vous êtes invité plus souvent ou moins souvent qu'alors chez des parents, des amis ou des connaissances ? [D'une manière générale, et si vous comparez la situation actuelle à ce qui se passait lorsque vous aviez 40 à 50 ans".
VARIABLE LABELS i65_1 "Dans notre société où tout évolue si vite, l'expérience des personnes comme moi n'intéresse guère les autres [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ".
VARIABLE LABELS i65_2 "À condition de «savoir passer la main», les personnes comme moi peuvent se rendre très utiles [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vo".
VARIABLE LABELS i65_3 "Pour les personnes comme moi, ce n'est guère important de faire attention à ses habits et à son apparence [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces".
VARIABLE LABELS i65_4 "Les personnes comme moi ne doivent pas trop demander à leurs enfants car ceux-ci ont leur travail et leur vie à mener [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesqu".
VARIABLE LABELS i65_5 "Les personnes comme moi ont meilleur temps de rester chez elles, on y aest plus tranquille et on n'a pas d'ennuis [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelle".
VARIABLE LABELS i65_6 "Dans notre société, on ne respecte plus guère les personnes comme moi [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_7 "Grâce au progrès, la vie à mon âge est plus facile aujourd'hui qu'autrefois [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_8 "Les enfants devraient se responsabiliser beaucoup plus de leurs parents quand ceux-ci sont de mon âge".
VARIABLE LABELS i65_9 "À condition d'être assez discrètes, les personnes comme moi peuvent rendre beaucoup de services [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions ".
VARIABLE LABELS i65_10 "Pour les personnes comme moi, aller voter est inutile [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_11 "Personne ne s'intéresse vraiment aux gens comme moi [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_12 "Il est urgent que les personnes comme moi s'organisent entre elles pour défendre leurs intérêts [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions".
VARIABLE LABELS i65_13 "Tout ce qui reste aux personnes comme moi c'est d'attendre la mort [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_14 "Les parents laissent beaucoup trop faire leurs enfants [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_15 "Les jeunes d'aujourd'hui on la vie beaucoup trop facile [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_16 "Pour réussir dans la vie, il faut avoir de la chance et aussi du piston [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_17 "La place de le femme est d'être à son foyer, dans son ménage [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_18 "Pour réussir dans la vie, il faut travailler dur [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_19 "Ma vie ne s'arrête pas avec la mort [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_20 "Après leur vie, les hommes seront jugés par Dieu [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_21 "Après leur vie, les hommes seront accueillis et pardonnés par Dieu [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît, avec lesquelles de ces opinions vous êtes d'accord.]".
VARIABLE LABELS i65_22 "Les personnes comme moi doivent profiter de cette phase de vie pour faire le point sur leur existence [Nous allons vous énoncer une série d'opinions qu'on entend parfois. Dites-nous, s'il vous plaît".
VARIABLE LABELS i66a "Qui trouve qu'on s'amuse bien avec vous ou qu'on peut bien jouer avec vous [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i66b "Qui vous demande sérieusement votre avis  [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i66c "Qui aime bien passer de longs moments avec vous [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i66d "Qui a pour vous une affection profonde".
VARIABLE LABELS i66e "Qui vous donne des marques de respect [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i66f "Qui compte sur vous pour maintenir l'esprit de famille [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i66g "Qui compte un peu sur vous pour arranger les brouilles, les disputes [Parmi votre parenté, y a-t-il quelqu'un (ou plusieurs personnes) :]".
VARIABLE LABELS i67 "Quel type de relations entretenez-vous avec vos voisins ?".
VARIABLE LABELS i68 "Avez-vous des amis intimes ?".
VARIABLE LABELS i69a1 "Où habite la première (commune) [Pensez maintenant, en dehors de votre famille et des personnes qui vivent avec vous, aux deux personnes auxquelles vous êtes le plus attaché]".
VARIABLE LABELS i69a2 "Où habite la deuxième (commune) [Pensez maintenant, en dehors de votre famille et des personnes qui vivent avec vous, aux deux personnes auxquelles vous êtes le plus attaché]".
VARIABLE LABELS i69b1 "[Première] À quelle fréquence les voyez-vous ?".
VARIABLE LABELS i69b2 "[Deuxième] À quelle fréquence les voyez-vous ?".
VARIABLE LABELS i69_nomb "Nombre de personnes citées".
VARIABLE LABELS i70 "Aimeriez-vous avoir plus d'amis ?".
VARIABLE LABELS i71 "Où situez-vous le revenu mensuel brut de votre ménage ? [Voici une échelle de revenu mensuel]".
VARIABLE LABELS i72a "Caisse de retraite (autre que l'AVS) [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72b "Salaire ou gain résultant d'un travail [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72c "Intérêts d'épargne; rentes; titres; assurance-vie; etc. [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72d_enf "Aides financières des enfants [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72d_aut "Aides financières d'organismes privés ou ecclésiastiques [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72e "Allocations cantonales ou communales [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72f "Produit d'une location (d'immeubles, de terrains, etc.) [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i72g "Autre [Bénéficiez-vous actuellement de l'une ou l'autre des sources de revenus suivantes ?]".
VARIABLE LABELS i73 "Avec votre revenu actuel, avez-vous de la peine à «nouer les deux bouts» ?".
VARIABLE LABELS i74 "Parmi les catégories suivantes (donner la carte), où siotuez-vous approximativement votre fortune ?".
VARIABLE LABELS i75 "À quel proportion de votre revenu d'avant la retraite estimez-vous que correspond votre revenu actuel ?".
VARIABLE LABELS i76 "Comment répartissez-vous les frais du ménage et du logement avec la personne (les personnes) qui vivent avec vous ? (en dehors du conjoint) Dans l'ensemble, est-ce plutôt vous qui leur payez une pension, plutôt eux qui paient po".
VARIABLE LABELS i77 "Quelle est votre confession ?".
VARIABLE LABELS i78 "Vous rattachez-vous à une communauté spirituelle ou religieuse qui n'appartienne ni à l'Église catholique, ni à l'Église protestante ?".
VARIABLE LABELS i79 "Vous arrive-t-il de rendre visite à (ou de recevoir la visite) d'un ecclésiastique ou d'un responsable d'un groupe religieux ?".
VARIABLE LABELS i80 "Événement 1 [Pouvez-vous nous indiquer les deux événements que vous avez vécus après la quarantaine, qui ont marqué votre vie et dont vous gardez le plus mauvais souvenir.]".
VARIABLE LABELS i80_age "À quel âge cela s'est-il produit ?".
VARIABLE LABELS i80b "Événement 2 [Pouvez-vous nous indiquer les deux événements que vous avez vécus après la quarantaine, qui ont marqué votre vie et dont vous gardez le plus mauvais souvenir.]".
VARIABLE LABELS i80b_age "À quel âge cela s'est-il produit ?".
VARIABLE LABELS i81 "Événement 1 [Pouvez-vous nous dire quels sont les deux événements qui vous avez vécus après la quarantaine, qui ont marqué votre vie et dont vous gardez le meilleur souvenir.]".
VARIABLE LABELS i81_age "À quel âge cela s'est-il produit ?".
VARIABLE LABELS i81b "Événement 1 [Pouvez-vous nous dire quels sont les deux événements qui vous avez vécus après la quarantaine, qui ont marqué votre vie et dont vous gardez le meilleur souvenir.]".
VARIABLE LABELS i81b_age "À quel âge cela s'est-il produit ?".
VARIABLE LABELS e1 "Où habite la personne interrogée ?".
VARIABLE LABELS e2 "À quel étage ?".
VARIABLE LABELS e3 "L'accès de l'habitation se fait-il :".
VARIABLE LABELS e4a "La personne avait-elle des problèmes de mémoire ?".
VARIABLE LABELS e4b "La personne avait-elle des difficultés d'audition ?".
VARIABLE LABELS e4c "La personne avait-elle des difficultés d'élocution ?".
VARIABLE LABELS e5 "Était-elle alitée, paralysée, ou devait-elle utiliser des moyens auxiliaires tels que béquilles par exemple ?".

*Définition des labels de valeurs

VALUE LABELS v2		
	1	"Homme du Valais"
	2	"Femme du Valais"
	3	"Homme de Genève"
	4	"Femme de Genève".
VALUE LABELS v3		
	1	"?"
	2	"?"
	3	"?"
	4	"En institution"
	5	"En institution".
VALUE LABELS v4		
	1	"Valais"
	2	"Genève".
VALUE LABELS q1_type		
	1	"Montagne Valais 1500 m. et plus"
	2	"Montagne Valais 1150-1500 m."
	3	"Montagne Valais 800-1150 m."
	4	"Plaine Valais"
	5	"Ville Valais"
	6	"Campagne Genève"
	7	"Agglomération Genève"
	8	"Ville Genève".
VALUE LABELS q2		
	1	"Homme"
	2	"Femme".
VALUE LABELS q3_const		
	1	"65-69"
	2	"70-74"
	3	"75-79"
	4	"80 et plus".
VALUE LABELS q4		
	-9	"Missing".
VALUE LABELS q4_regro		
	-9	"Missing".
VALUE LABELS q5		
	1	"Célibataire"
	2	"Marié(e)"
	3	"Veuf, veuve"
	4	"Séparé(e)"
	5	"Divorcé(e)".
VALUE LABELS q6		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6v1		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6v2		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6d1		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6d2		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6r1		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q6r2		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q7		
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q8		
	1	"Oui"
	2	"Non".
VALUE LABELS q8_nombr		
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_1sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_1nais		
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_1dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_1domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_1prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_1pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_1peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_2sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_2nais		
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_2dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_2domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_2prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_2pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_2peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_3sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_3nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_3dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_3domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_3prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_3pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_3peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_4sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_4nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_4dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_4domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_4prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_4pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_4peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_5sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_5nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_5dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_5domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_5prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_5pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_5peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_6sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_6nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_6dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_6domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_6prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_6pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_6peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_7sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_7nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_7dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_7domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_7prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_7pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_7peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_8sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_8nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_8dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_8domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_8prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_8pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_8peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q9_9sex		
	-7	"INAP"
	-1	"NSP"
	1	"Fils"
	2	"Fille".
VALUE LABELS q9_9nais		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_9dec		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_9domi		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q9_9prof		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_9pr_1		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS q9_9peti		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q10		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS q11a		
	-9	"Missing".
VALUE LABELS q11b		
	-9	"Missing".
VALUE LABELS q12a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q12b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q13a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q13b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q14		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q15a		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15b		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15c		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15d		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15e		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15f		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15g		
	-9	"Missing"
	1	"Souvent"
	2	"Parfois"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q15h		
	-9	"Missing"
	-2	"NR"
	1	"Oui"
	2	"Non".
VALUE LABELS q16		
	-9	"Missing"
	1	"Oui, très régulièrement"
	2	"Oui, une fois ou l'autre"
	3	"Non, jamais"
	4	"Il n'y a pas de situation de ce genre dans ma famille".
VALUE LABELS q17		
	-9	"Missing".
VALUE LABELS q18a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q18b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q18c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q18d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q18e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q18f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q19a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q19b		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q20		
	-7	"INAP"
	1	"Officier"
	2	"Sous-officier"
	3	"Soldat"
	4	"Service complémentaire uniquement"
	5	"Pas fait de service militaire".
VALUE LABELS q21		
	-9	"Missing"
	1	"Non, jamais"
	2	"Oui, c'est arrivé une fois"
	3	"Oui, c'est arrivé plusieurs fois".
VALUE LABELS q22a		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q22b		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q22c		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q22d		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q22e		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP".
VALUE LABELS q23a		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Accident"
	2	"Maladie"
	3	"Autre cause".
VALUE LABELS q23b		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS q23c		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui, totalement"
	2	"Oui, avec gênes occasionelles ou mineures"
	3	"Non".
VALUE LABELS q23d		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Non, pas du tout"
	2	"Non, presque pas"
	3	"Oui, durant plusieurs mois"
	4	"Oui, durant plusieurs années".
VALUE LABELS q24a		
	-9	"Missing"
	-7	"INAP"
	1	"Accident"
	2	"Maladie"
	3	"Autre cause".
VALUE LABELS q24b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS q24c		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui, totalement"
	2	"Oui, avec gênes occasionelles ou mineures"
	3	"Non".
VALUE LABELS q24d		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Non, pas du tout"
	2	"Non, presque pas"
	3	"Oui, durant plusieurs mois"
	4	"Oui, durant plusieurs années".
VALUE LABELS q25a		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25b		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25c		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25d		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25e		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25f		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25g		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25h		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25i		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25j		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q25k		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q26a		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q26b		
	-9	"Missing"
	1	"Toujours"
	2	"Souvent"
	3	"Rarement"
	4	"Jamais".
VALUE LABELS q27a		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27b		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27c		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27d		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27e		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27f		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27g		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27h		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27i		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27j		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27k		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27l		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q27m		
	-9	"Missing"
	1	"Oui, sans difficulté majeure"
	2	"Oui, mais avec difficulté"
	3	"Non".
VALUE LABELS q28		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q28_mens		
	-9	"Missing"
	1	"Aucune"
	2	"Moins d'une"
	3	"Une environ"
	4	"Deux"
	5	"Trois ou quatre"
	6	"Cinq et plus".
VALUE LABELS q29a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29g		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29h		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29i		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q29j		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q30		
	-9	"Missing"
	1	"Non"
	2	"Oui, dans une"
	3	"Oui, dans plusieurs".
VALUE LABELS q31		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q32		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q33		
	-9	"Missing"
	-7	"INAP"
	1	"Tous les jours ou presque"
	2	"Quelques fois par semaine"
	3	"Une fois par semaine environ"
	4	"Quelques fois par mois"
	5	"Pratiquement jamais"
	6	"Jamais".
VALUE LABELS q34		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non, je n'en ai jamais eu".
VALUE LABELS q34a		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS q34b		
	-9	"Missing"
	-7	"INAP"
	1	"Je n'en avais plus besoin"
	2	"Pour des raisons de santé"
	3	"Pour des raisons financières"
	4	"Pour d'autres raisons".
VALUE LABELS q35		
	-9	"Missing"
	1	"Toujours habité ici"
	2	"Plus de 50 ans"
	3	"Plus de 20 ans"
	4	"Plus de 15 ans"
	5	"Plus de 10 ans"
	6	"Plus de 5 ans"
	7	"Entre 2 et 5 ans"
	8	"Moins de 2 ans".
VALUE LABELS q36		
	-9	"Missing"
	1	"Toujours habité ici"
	2	"Plus de 50 ans"
	3	"Plus de 20 ans"
	4	"Plus de 15 ans"
	5	"Plus de 10 ans"
	6	"Plus de 5 ans"
	7	"Entre 2 et 5 ans"
	8	"Moins de 2 ans".
VALUE LABELS q37a		
	-9	"Missing".
VALUE LABELS q37b		
	-9	"Missing".
VALUE LABELS q38a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q38g		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS q39a		
	-9	"Missing"
	1	"L'ancien"
	2	"Même chose"
	3	"Le nouveau".
VALUE LABELS q39b		
	-9	"Missing"
	1	"L'ancien"
	2	"Même chose"
	3	"Le nouveau".
VALUE LABELS q39c		
	-9	"Missing"
	1	"L'ancien"
	2	"Même chose"
	3	"Le nouveau".
VALUE LABELS q39d		
	-9	"Missing"
	1	"L'ancien"
	2	"Même chose"
	3	"Le nouveau".
VALUE LABELS q39e		
	-9	"Missing"
	1	"L'ancien"
	2	"Même chose"
	3	"Le nouveau".
VALUE LABELS q40a		
	-9	"Missing"
	1	"Moins de 5 minutes"
	2	"Moins d'1/4 heure"
	3	"Moins d'1‎/2 heure"
	4	"Je n'y vais jamais"
	5	"Je ne peux plus y aller à pied".
VALUE LABELS q40b		
	-9	"Missing"
	1	"Moins de 5 minutes"
	2	"Moins d'1/4 heure"
	3	"Moins d'1‎/2 heure"
	4	"Je n'y vais jamais"
	5	"Je ne peux plus y aller à pied".
VALUE LABELS q40c		
	-9	"Missing"
	1	"Moins de 5 minutes"
	2	"Moins d'1/4 heure"
	3	"Moins d'1‎/2 heure"
	4	"Je n'y vais jamais"
	5	"Je ne peux plus y aller à pied".
VALUE LABELS ie1		
	-9	"Missing"
	1	"Complètement et correctement rempli"
	2	"Complètement mais mal rempli"
	3	"En partie mais correctement rempli"
	4	"En partie et mal rempli"
	5	"Pas du tout rempli".
VALUE LABELS ie3		
	-9	"Missing"
	1	"Dans un logement privé : villa"
	2	"Dans un logement privé : appartement"
	3	"Dans un logement à encadrement médico-social"
	4	"Dans une pension"
	5	"Dans une institution"
	6	"Dans un hôpital".
VALUE LABELS i2		
	-9	"Missing"
	1	"Propriétaire ou co-propriétaire"
	2	"Locataire"
	3	"Sous-locataire d'une chambre mise à disposition".
VALUE LABELS i3		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i4		
	-9	"Missing"
	-7	"INAP"
	1	"Oui, j'aimerais beaucoup, je le souhaiterais vraiment"
	2	"Oui, parfois je le souhaite"
	3	"Non, je ne le souhaite pas".
VALUE LABELS i4a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i4b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i4c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i4d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i4e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5a		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i5ba		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5bb		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5bc		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5bd		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5be		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5bf		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i5bg		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i6a		
	-9	"Missing"
	-7	"INAP"
	1	"Chez vous"
	2	"Chez eux (lui, elle)"
	3	"Autre situation".
VALUE LABELS i6b		
	-9	"Missing"
	-7	"INAP"
	1	"Moins de 2 ans"
	2	"2 à 5 ans"
	3	"6 à 10 ans"
	4	"De 11 à 20 ans"
	5	"Plus de 20 ans"
	6	"Toujours habité ensemble".
VALUE LABELS i7		
	-9	"Missing"
	-7	"INAP"
	1	"Une ou une et demie"
	2	"Deux ou deux et demie"
	3	"Trois ou trois et demie"
	4	"Quatre ou quatre et demie"
	5	"Cinq et plus".
VALUE LABELS i8		
	-9	"Missing"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS i9		
	-9	"Missing"
	1	"Primaire"
	2	"Secondaire inférieure"
	3	"Professionnelle (pour apprentis)"
	4	"Secondaire supérieure"
	5	"École technique supérieure"
	6	"École sociale, école normale"
	7	"Université, polytechnique".
VALUE LABELS i10_annee		
	-9	"Missing".
VALUE LABELS i10_comm		
	-9	"Missing".
VALUE LABELS i10_cant		
	-9	"Missing".
VALUE LABELS i11		
	-9	"Missing"
	1	"Oui, toute l'année, à plein temps"
	2	"Oui, toute l'année, à 3‎/4 temps environ"
	3	"Oui, toute l'année, à mi-temps environ"
	4	"Oui, toute l'année, à 1‎/4 temps environ"
	5	"Oui, quelques mois par an, mais régulièrement toutes les années"
	6	"Oui, très occasionnellement (un mois ou moins par an en moyenne)"
	7	"Non".
VALUE LABELS i12a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i12b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i12c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i12d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i13		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i14a		
	-9	"Missing"
	-7	"INAP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS i14b		
	-9	"Missing"
	-7	"INAP"
	1	"Indépendant, patron"
	2	"Salarié"
	3	"Entreprise familiale mais sans être patron".
VALUE LABELS i14c		
	-9	"Missing"
	-7	"INAP"
	1	"Travaillait seul ou aucun"
	2	"Moins de 5"
	3	"6 à 20"
	4	"21 à 50"
	5	"Plus de 50".
VALUE LABELS i14d		
	-9	"Missing"
	-7	"INAP"
	1	"À la tâche, à la pièce"
	2	"À l'heure"
	3	"Au mois"
	4	"Autre".
VALUE LABELS i14e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i14f		
	-9	"Missing"
	-7	"INAP"
	1	"Toute l'année"
	2	"En saison"
	3	"Occasionnellement".
VALUE LABELS i15a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15g		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15h		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15i		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15j		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i15k		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i16		
	-9	"Missing"
	-7	"INAP"
	1	"Non"
	2	"Oui, dans une"
	3	"Oui, dans plusieurs".
VALUE LABELS i17a		
	-9	"Missing"
	1	"Très bon"
	2	"Bon"
	3	"Satisfaisant"
	4	"Plutôt mauvais"
	5	"Mauvais".
VALUE LABELS i17b		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i18a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i18b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i18c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i18d		
	-9	"Missing"
	-7	"INAP"
	1	"Plus"
	2	"Égal"
	3	"Moins"
	4	"Dû arrêter"
	5	"Arrêté".
VALUE LABELS i19		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i19a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i19b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i19c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i19d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i20		
	-9	"Missing"
	-7	"INAP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS i20a		
	-9	"Missing"
	-7	"INAP"
	1	"Indépendant, patron"
	2	"Salarié"
	3	"Entreprise familiale, mais pas patron".
VALUE LABELS i20b		
	-9	"Missing"
	-7	"INAP"
	1	"Travaillait seul, ou aucun"
	2	"Moins de 5"
	3	"6 à 20"
	4	"21 à 50"
	5	"Plus de 50".
VALUE LABELS i20c		
	-9	"Missing"
	-7	"INAP"
	1	"À la tâche, à la pièce"
	2	"À l'heure"
	3	"Au mois"
	4	"Autre".
VALUE LABELS i21a		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i21b		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i21c		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i21d		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i21e		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i21f		
	-9	"Missing"
	-7	"INAP"
	1	"Très intéressant"
	2	"Intéressant"
	3	"Plus ou moins intéressant"
	4	"Peu intéressant"
	5	"Pas du tout intéressant".
VALUE LABELS i22a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i22b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i23a		
	-9	"Missing"
	-7	"INAP"
	-2	"NR"
	-1	"Ne convient pas"
	1	"Améliorée"
	2	"Restée la même"
	3	"Détériorée".
VALUE LABELS i23b		
	-9	"Missing"
	-7	"INAP"
	-2	"NR"
	-1	"Ne convient pas"
	1	"Plus varié"
	2	"Resté le même"
	3	"Plus monotone".
VALUE LABELS i23c		
	-9	"Missing"
	-7	"INAP"
	-2	"NR"
	-1	"Ne convient pas"
	1	"Plus pénible"
	2	"Resté le même"
	3	"Moins pénible".
VALUE LABELS i23d		
	-9	"Missing"
	-7	"INAP"
	-2	"NR"
	-1	"Ne convient pas"
	1	"Améliorées"
	2	"Restées les mêmes"
	3	"Détériorées".
VALUE LABELS i24		
	-9	"Missing"
	-7	"INAP"
	-1	"Sans objet"
	1	"Oui, et je n'ai pas repris d'autre travail"
	2	"Oui, mais j'ai repris ensuite un autre travail"
	3	"Non, j'exerce toujours cette même profession".
VALUE LABELS i24_age		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i25		
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i25_age		
	-7	"INAP".
VALUE LABELS i26		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i26b		
	-9	"Missing"
	-7	"INAP"
	1	"Anticipée"
	2	"Âge légal"
	3	"Repoussée".
VALUE LABELS i27a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i27b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i27c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i27d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i27e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i28a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i28b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i28c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i28d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i28e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i29a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui, régulièrement"
	2	"Oui, à l'occasion"
	3	"Par hasard"
	4	"Non, pas du tout".
VALUE LABELS i29b		
	-9	"Missing"
	-7	"INAP"
	1	"Chez vous"
	2	"Chez eux"
	3	"Au café, au restaurant"
	4	"Au cours des promenades"
	5	"Dans un club, une association"
	6	"Autre".
VALUE LABELS i30a		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i30b		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i30c		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i30d		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i30e		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i30f		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31a		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31b		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31c		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31d		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31e		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i31f		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Pas du tout".
VALUE LABELS i32		
	-9	"Missing"
	-7	"INAP"
	1	"Oui, gardé"
	2	"Oui, repris"
	3	"Non".
VALUE LABELS i33		
	-9	"Missing"
	-7	"INAP"
	1	"La même activité professionnelle et du même temps de travail que vous aviez à 65 ans ?"
	2	"La même activité profesionnelle que vous aviez avant 65 ans, mais avec un horaire réduit ?"
	3	"Une nouvelle activité rémunérée à plein temps ?"
	4	"Une nouvelle activité rémunérée à temps partiel ?".
VALUE LABELS i34_prin		
	-9	"Missing"
	-7	"INAP"
	1	"Nécessité financière"
	2	"Intérêt professionnel"
	3	"Besoin de contact avec des collègues de travail"
	4	"Désir d'une vie bien réglée"
	5	"Autre"
	6	"Ne sait pas".
VALUE LABELS i34_seco		
	-9	"Missing"
	-7	"INAP"
	1	"Nécessité financière"
	2	"Intérêt professionnel"
	3	"Besoin de contact avec des collègues de travail"
	4	"Désir d'une vie bien réglée"
	5	"Autre"
	6	"Ne sait pas".
VALUE LABELS i35		
	-9	"Missing"
	-7	"INAP"
	-1	"Ne sait pas"
	1	"Retraite : dans moins d'un an"
	2	"Retraite : d'ici 2 à 3 ans"
	3	"Retraite : d'ici 4 à 5 ans"
	4	"Retraite : dans plus de 5 ans"
	5	"Retraite : à une date indéterminée"
	6	"Continuer".
VALUE LABELS i36		
	-9	"Missing"
	1	"Célibataire"
	2	"Marié(e)"
	3	"Veuf, veuve"
	4	"Séparé(e), divorcé(e)".
VALUE LABELS i37a		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37b		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37c		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37d		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37e		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37f		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37g		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i37h		
	-9	"Missing"
	-7	"INAP"
	-2	"NDR"
	1	"Très graves"
	2	"Importants"
	3	"Mineurs"
	4	"Pas un problème".
VALUE LABELS i38		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38c		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38d		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38f		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38g		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i38h		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i39		
	-9	"Missing"
	-7	"INAP"
	1	"N'a jamais travaillé"
	2	"Ne travaillait pas à ce moment-là"
	3	"Actif(ve) à plein temps"
	4	"Actif(ve) plus de mi-temps"
	5	"Actif(ve) mi-temps"
	6	"Actif(ve) moins de mi-temps".
VALUE LABELS i39_anne		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i39_repr		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i39_prof		
	-9	"Missing"
	-7	"INAP"
	1	"Manoeuvres et ouvriers non qualifiés"
	2	"Ouvriers qualifiés"
	3	"Contremaîtres"
	4	"Agriculteurs, viticulteurs"
	5	"Employés subalternes non qualifiés"
	6	"Employés subalternes qualifiés"
	7	"Cadres moyens"
	8	"Petits commerçants et artisans"
	9	"Moyens industriels et commerçants"
	10	"Cadres supérieurs"
	11	"Dirigeants et membres des professions libérales"
	12	"Aides au conjoint"
	13	"Autres".
VALUE LABELS i39_pr_1		
	-9	"Missing"
	-7	"INAP"
	1	"Indépendant, patron"
	2	"Salarié"
	3	"Entreprise familiale, mais pas patron".
VALUE LABELS i39_pr_2		
	-9	"Missing"
	-7	"INAP"
	1	"Travaillait seul(e) ou dépendant uniquement"
	2	"Moins de 5"
	3	"6 à 20 environ"
	4	"Plus de 20".
VALUE LABELS i39_pr_3		
	-9	"Missing"
	-7	"INAP"
	1	"À la tâche, à la pièce"
	2	"À l'heure"
	3	"Au mois"
	4	"Autre".
VALUE LABELS i40		
	-9	"Missing"
	-7	"INAP"
	-4	"Pas encore prise"
	-3	"Sans objet".
VALUE LABELS i41		
	-9	"Missing"
	-7	"INAP"
	1	"D'une retraite anticipée"
	2	"D'une retraite à l'âge fixé légalement"
	3	"D'une retraite repoussée".
VALUE LABELS i43		
	-9	"Missing"
	-7	"INAP"
	1	"Toujours aussi gênant pour vous"
	2	"Gênant, mais beaucoup moins"
	3	"Ou cela ne vous gêne-t-il plus du tout".
VALUE LABELS i44a		
	-9	"Missing"
	-7	"INAP"
	1	"Oui, à plein temps"
	2	"Oui, à temps partiel"
	3	"Non".
VALUE LABELS i44b		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i45		
	-9	"Missing"
	1	"Très bon"
	2	"Bon"
	3	"Satisfaisant"
	4	"Plutôt mauvais"
	5	"Mauvais".
VALUE LABELS i46		
	-9	"Missing"
	-1	"Ne sait pas"
	1	"Meilleur"
	2	"Équivalent"
	3	"Pire".
VALUE LABELS i47a1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47a2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47a3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47a4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47a5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47a6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47b1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47b2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47b3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47b4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47b5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47b6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47c1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47c2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47c3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47c4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47c5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47c6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47d1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47d2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47d3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47d4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47d5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47d6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47e1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47e2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47e3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47e4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47e5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47e6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47f1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47f2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47f3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47f4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47f5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47f6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47g1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47g2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47g3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47g4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47g5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47g6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47h1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47h2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47h3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47h4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47h5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47h6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47i1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47i2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47i3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47i4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47i5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47i6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47j1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47j2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47j3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47j4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47j5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47j6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i47k1		
	-9	"Missing"
	1	"Non"
	2	"Oui".
VALUE LABELS i47k2		
	-9	"Missing"
	-7	"INAP"
	1	"Très fréquents"
	2	"Plus ou moins fréquents"
	3	"Peu fréquents".
VALUE LABELS i47k3		
	-9	"Missing"
	-7	"INAP"
	1	"Légers"
	2	"Assez marqués"
	3	"Très marqués".
VALUE LABELS i47k4		
	-9	"Missing"
	-7	"INAP"
	1	"Beaucoup"
	2	"Un peu"
	3	"Non".
VALUE LABELS i47k5		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Rarement"
	3	"Non".
VALUE LABELS i47k6		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i51f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i52a		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i52b		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i52c		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i52d		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i52e		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i52f		
	-9	"Missing"
	1	"Oui"
	2	"En partie"
	3	"Non".
VALUE LABELS i53		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i53_type		
	-9	"Missing"
	-7	"INAP"
	1	"Mari‎/femme"
	2	"Parents (père-mère)"
	3	"Beaux-parents"
	4	"Enfant"
	5	"Petit-enfant"
	6	"Autre parent"
	7	"Ami"
	8	"Voisin"
	9	"Autre".
VALUE LABELS i54a		
	-9	"Missing"
	1	"Non"
	2	"Tous les jours"
	3	"Quelques fois par semaine"
	4	"Une fois par semaine"
	5	"Une fois par quinzaine"
	6	"Une fois par mois"
	7	"Plus rare".
VALUE LABELS i54b		
	-9	"Missing"
	1	"Non"
	2	"Tous les jours"
	3	"Quelques fois par semaine"
	4	"Une fois par semaine"
	5	"Une fois par quinzaine"
	6	"Une fois par mois"
	7	"Plus rare".
VALUE LABELS i54c		
	-9	"Missing"
	1	"Non"
	2	"Tous les jours"
	3	"Quelques fois par semaine"
	4	"Une fois par semaine"
	5	"Une fois par quinzaine"
	6	"Une fois par mois"
	7	"Plus rare".
VALUE LABELS i54d		
	-9	"Missing"
	1	"Non"
	2	"Tous les jours"
	3	"Quelques fois par semaine"
	4	"Une fois par semaine"
	5	"Une fois par quinzaine"
	6	"Une fois par mois"
	7	"Plus rare".
VALUE LABELS i54e		
	-9	"Missing"
	1	"Non"
	2	"Tous les jours"
	3	"Quelques fois par semaine"
	4	"Une fois par semaine"
	5	"Une fois par quinzaine"
	6	"Une fois par mois"
	7	"Plus rare".
VALUE LABELS i55a		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55b		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55c		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55d		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55e		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55f		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i55g		
	-9	"Missing"
	1	"Toujours vous"
	2	"Partagé: vous et autre"
	3	"Toujours autre personne"
	4	"Donné à l'extérieur".
VALUE LABELS i56a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i56g		
	-9	"Missing"
	-7	"INAP"
	1	"Dans la même maison (chez des voisins)"
	2	"Ou devez-vous sortir de la maison pour aller chez des voisins"
	3	"Ou sortir de la maison pour aller à une cabine publique".
VALUE LABELS i57a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i57b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i57c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i57d		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i57e		
	-9	"Missing"
	-7	"INAP"
	1	"Oui, chez des parents"
	2	"Oui, chez des voisins"
	3	"Oui, au café"
	4	"Non".
VALUE LABELS i58		
	-9	"Missing"
	1	"Tous les jours"
	2	"Un jour sur deux"
	3	"Une à deux fois par semaine"
	4	"Deux à trois fois par semaine"
	5	"Plus rarement"
	6	"Jamais".
VALUE LABELS i59		
	0	"Jamais"
	1	"Tous les jours ou presque, plus de 4 heures"
	2	"Tous les jours ou presque, 3 à 4 heures"
	3	"Tous les jours ou presque, 2 à 3 heures"
	4	"Tous les jours ou presque, 1 à 2 heures"
	5	"Tous les jours ou presque, moins d'une heure"
	6	"Environ un jour sur deux"
	7	"Une fois par semaine environ"
	8	"Quelques fois par mois"
	9	"Moins d'une fois par mois, très rarement".
VALUE LABELS i60		
	-9	"Missing"
	1	"Oui, deux ou plus"
	2	"Oui, un"
	3	"Non".
VALUE LABELS i61		
	-9	"Missing"
	1	"Ne lit pas"
	2	"Environ 1/4 d'heure"
	3	"Environ 1‎/2 heure"
	4	"Environ une heure"
	5	"Environ deux heures"
	6	"Environ trois heures"
	7	"Quatre heures ou plus".
VALUE LABELS i62ac_01		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_01		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_01		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_02		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_02		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_02		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_03		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_03		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_03		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_04		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_04		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_04		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_05		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_05		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_05		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_06		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_06		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_06		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_07		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_07		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_07		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_08		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_08		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_08		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_09		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_09		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_09		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_10		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_10		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_10		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_11		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_11		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_11		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_12		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_12		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_12		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_13		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_13		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_13		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_14		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_14		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_14		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_15		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_15		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_15		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_16		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_16		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_16		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62ac_17		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i62av_17		
	-9	"Missing"
	-7	"INAP"
	1	"Oui"
	2	"Non".
VALUE LABELS i62co_17		
	-9	"Missing"
	-7	"INAP"
	1	"Actuel + que 40‎/50 ans"
	2	"Egal"
	3	"Actuel - que 40‎/50 ans".
VALUE LABELS i62radio		
	-9	"Missing"
	1	"Oui".
VALUE LABELS i63_1		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_2		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_3		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_4		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_5		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_6		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_7		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_8		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_9		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_10		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_11		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_12		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_13		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i63_14		
	-9	"Missing"
	1	"Non"
	2	"Au moins une fois par an"
	3	"Au moins une fois par mois"
	4	"Au moins une fois par semaine"
	5	"Tous les jours ou presque".
VALUE LABELS i64a		
	-9	"Missing"
	-7	"INAP"
	1	"Plus qu'avant"
	2	"Même chose"
	3	"Moins qu'avant"
	4	"Ne reçoit pratiquement jamais".
VALUE LABELS i64b		
	-9	"Missing"
	-7	"INAP"
	1	"Plus qu'avant"
	2	"Même chose"
	3	"Moins qu'avant"
	4	"N'est pratiquement jamais invité".
VALUE LABELS i65_1		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_2		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_3		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_4		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_5		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_6		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_7		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_8		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_9		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_10		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_11		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_12		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_13		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_14		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_15		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_16		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_17		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_18		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_19		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_20		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_21		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i65_22		
	-9	"Missing"
	-7	"INAP"
	1	"D'accord"
	2	"Pas d'accord".
VALUE LABELS i66a		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66b		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66c		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66d		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66e		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66f		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i66g		
	-9	"Missing"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i67		
	-9	"Missing"
	1	"Bonjour-bonsoir"
	2	"Conversations occasionnelles"
	3	"Vous vous rendez de petits services"
	4	"Vous vous rendez des visites"
	5	"Vous faites des sorties ou des promenades avec eux"
	6	"Autre".
VALUE LABELS i68		
	-9	"Missing"
	1	"Non, je n'en ai aucun"
	2	"J'en ai un"
	3	"J'en ai deux ou trois"
	4	"J'en ai plusieurs".
VALUE LABELS i69a1		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i69a2		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i69b1		
	-9	"Missing"
	-7	"INAP"
	1	"Tous les jours"
	2	"Deux, trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS i69b2		
	-9	"Missing"
	-7	"INAP"
	1	"Tous les jours"
	2	"Deux, trois fois par semaine"
	3	"Chaque semaine"
	4	"Chaque quinzaine"
	5	"Chaque mois"
	6	"Quelques fois dans l'année"
	7	"Une fois par an environ"
	8	"Plus rarement".
VALUE LABELS i69_nomb		
	-9	"Missing"
	1	"Deux"
	2	"Une seule"
	3	"Aucune".
VALUE LABELS i70		
	-9	"Missing"
	-7	"INAP"
	-1	"NSP"
	1	"Oui"
	2	"Non".
VALUE LABELS i71		
	-9	"Missing".
VALUE LABELS i72a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72d_enf		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72d_aut		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72e		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72f		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i72g		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i73		
	-9	"Missing"
	1	"Non, pas du tout"
	2	"Non, il faut se contenter de ce qu'on a"
	3	"C'est parfois difficile, mais dans l'ensemble ça va"
	4	"Oui, un peu de peine"
	5	"Oui, beaucoup de peine".
VALUE LABELS i74		
	-9	"Missing"
	-2	"NR"
	1	"Rien ou presque rien"
	2	"Moins de 30'000.-"
	3	"Entre 30'000.- et 100'000.-"
	4	"Plus de 100'000.-".
VALUE LABELS i75		
	-9	"Missing"
	1	"Moins de 50%"
	2	"Environ 50%"
	3	"Environ 60%"
	4	"Environ 70%"
	5	"Environ 80%"
	6	"Plus de 80%".
VALUE LABELS i76		
	-9	"Missing"
	1	"Plutôt moi qui paie une pension"
	2	"Plutôt eux qui paient pour moi"
	3	"Budgets séparés"
	4	"Autre".
VALUE LABELS i77		
	-9	"Missing"
	1	"Catholique"
	2	"Protestant - Église nationale protestante (GE)"
	3	"Protestant - Église réformée évangélique du Valais"
	4	"Protestant - autre"
	5	"Autre"
	6	"Sans".
VALUE LABELS i78		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS i79		
	-9	"Missing"
	1	"Jamais"
	2	"Accidentellement"
	3	"Une fois l'an"
	4	"Quelques fois par an"
	5	"Tous les mois"
	6	"Toutes les semaines"
	7	"Plusieurs fois par semaine".
VALUE LABELS i80		
	-9	"Missing".
VALUE LABELS i80_age		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i80b		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i80b_age		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i81		
	-9	"Missing".
VALUE LABELS i81_age		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i81b		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS i81b_age		
	-9	"Missing"
	-7	"INAP".
VALUE LABELS e1		
	-9	"Missing"
	1	"Ferme isolée"
	2	"Hameau"
	3	"Village"
	4	"Ville".
VALUE LABELS e2		
	-9	"Missing".
VALUE LABELS e3		
	-9	"Missing"
	1	"Si c'est au rez-de-chaussée, par une ou plusieurs marches: oui"
	2	"Si c'est au rez-de-chaussée, par une ou plusieurs marches: non"
	3	"Si c'est à un autre étage: escalier"
	4	"Si c'est à un autre étage: ascenseur".
VALUE LABELS e4a		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS e4b		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS e4c		
	-9	"Missing"
	1	"Oui"
	2	"Non".
VALUE LABELS e5		
	-9	"Missing"
	1	"Oui"
	2	"Non".

* Définition de la mesure des variables

VARIABLE LEVEL v1(NOMINAL).
VARIABLE LEVEL v2(NOMINAL).
VARIABLE LEVEL v3(NOMINAL).
VARIABLE LEVEL v4(NOMINAL).
VARIABLE LEVEL q1(NOMINAL).
VARIABLE LEVEL q1_type(NOMINAL).
VARIABLE LEVEL q2(NOMINAL).
VARIABLE LEVEL q3(SCALE).
VARIABLE LEVEL q3_age(SCALE).
VARIABLE LEVEL q3_const(ORDINAL).
VARIABLE LEVEL q4(NOMINAL).
VARIABLE LEVEL q4_regro(NOMINAL).
VARIABLE LEVEL q5(NOMINAL).
VARIABLE LEVEL q6(SCALE).
VARIABLE LEVEL q6v1(SCALE).
VARIABLE LEVEL q6v2(SCALE).
VARIABLE LEVEL q6d1(SCALE).
VARIABLE LEVEL q6d2(SCALE).
VARIABLE LEVEL q6r1(SCALE).
VARIABLE LEVEL q6r2(SCALE).
VARIABLE LEVEL q7(SCALE).
VARIABLE LEVEL q8(NOMINAL).
VARIABLE LEVEL q8_nombr(SCALE).
VARIABLE LEVEL q9_1sex(NOMINAL).
VARIABLE LEVEL q9_1nais(SCALE).
VARIABLE LEVEL q9_1dec(SCALE).
VARIABLE LEVEL q9_1domi(NOMINAL).
VARIABLE LEVEL q9_1prof(NOMINAL).
VARIABLE LEVEL q9_1pr_1(NOMINAL).
VARIABLE LEVEL q9_1peti(NOMINAL).
VARIABLE LEVEL q9_2sex(NOMINAL).
VARIABLE LEVEL q9_2nais(SCALE).
VARIABLE LEVEL q9_2dec(SCALE).
VARIABLE LEVEL q9_2domi(NOMINAL).
VARIABLE LEVEL q9_2prof(NOMINAL).
VARIABLE LEVEL q9_2pr_1(NOMINAL).
VARIABLE LEVEL q9_2peti(NOMINAL).
VARIABLE LEVEL q9_3sex(NOMINAL).
VARIABLE LEVEL q9_3nais(SCALE).
VARIABLE LEVEL q9_3dec(SCALE).
VARIABLE LEVEL q9_3domi(NOMINAL).
VARIABLE LEVEL q9_3prof(NOMINAL).
VARIABLE LEVEL q9_3pr_1(NOMINAL).
VARIABLE LEVEL q9_3peti(NOMINAL).
VARIABLE LEVEL q9_4sex(NOMINAL).
VARIABLE LEVEL q9_4nais(SCALE).
VARIABLE LEVEL q9_4dec(SCALE).
VARIABLE LEVEL q9_4domi(NOMINAL).
VARIABLE LEVEL q9_4prof(NOMINAL).
VARIABLE LEVEL q9_4pr_1(NOMINAL).
VARIABLE LEVEL q9_4peti(NOMINAL).
VARIABLE LEVEL q9_5sex(NOMINAL).
VARIABLE LEVEL q9_5nais(SCALE).
VARIABLE LEVEL q9_5dec(SCALE).
VARIABLE LEVEL q9_5domi(NOMINAL).
VARIABLE LEVEL q9_5prof(NOMINAL).
VARIABLE LEVEL q9_5pr_1(NOMINAL).
VARIABLE LEVEL q9_5peti(NOMINAL).
VARIABLE LEVEL q9_6sex(NOMINAL).
VARIABLE LEVEL q9_6nais(SCALE).
VARIABLE LEVEL q9_6dec(SCALE).
VARIABLE LEVEL q9_6domi(NOMINAL).
VARIABLE LEVEL q9_6prof(NOMINAL).
VARIABLE LEVEL q9_6pr_1(NOMINAL).
VARIABLE LEVEL q9_6peti(NOMINAL).
VARIABLE LEVEL q9_7sex(NOMINAL).
VARIABLE LEVEL q9_7nais(SCALE).
VARIABLE LEVEL q9_7dec(SCALE).
VARIABLE LEVEL q9_7domi(NOMINAL).
VARIABLE LEVEL q9_7prof(NOMINAL).
VARIABLE LEVEL q9_7pr_1(NOMINAL).
VARIABLE LEVEL q9_7peti(NOMINAL).
VARIABLE LEVEL q9_8sex(NOMINAL).
VARIABLE LEVEL q9_8nais(SCALE).
VARIABLE LEVEL q9_8dec(SCALE).
VARIABLE LEVEL q9_8domi(NOMINAL).
VARIABLE LEVEL q9_8prof(NOMINAL).
VARIABLE LEVEL q9_8pr_1(NOMINAL).
VARIABLE LEVEL q9_8peti(NOMINAL).
VARIABLE LEVEL q9_9sex(NOMINAL).
VARIABLE LEVEL q9_9nais(SCALE).
VARIABLE LEVEL q9_9dec(SCALE).
VARIABLE LEVEL q9_9domi(NOMINAL).
VARIABLE LEVEL q9_9prof(NOMINAL).
VARIABLE LEVEL q9_9pr_1(NOMINAL).
VARIABLE LEVEL q9_9peti(NOMINAL).
VARIABLE LEVEL q10(NOMINAL).
VARIABLE LEVEL q11a(SCALE).
VARIABLE LEVEL q11b(SCALE).
VARIABLE LEVEL q12a(NOMINAL).
VARIABLE LEVEL q12b(NOMINAL).
VARIABLE LEVEL q13a(NOMINAL).
VARIABLE LEVEL q13b(NOMINAL).
VARIABLE LEVEL q14(NOMINAL).
VARIABLE LEVEL q15a(NOMINAL).
VARIABLE LEVEL q15b(NOMINAL).
VARIABLE LEVEL q15c(NOMINAL).
VARIABLE LEVEL q15d(NOMINAL).
VARIABLE LEVEL q15e(NOMINAL).
VARIABLE LEVEL q15f(NOMINAL).
VARIABLE LEVEL q15g(NOMINAL).
VARIABLE LEVEL q15h(NOMINAL).
VARIABLE LEVEL q16(NOMINAL).
VARIABLE LEVEL q17(SCALE).
VARIABLE LEVEL q18a(NOMINAL).
VARIABLE LEVEL q18b(NOMINAL).
VARIABLE LEVEL q18c(NOMINAL).
VARIABLE LEVEL q18d(NOMINAL).
VARIABLE LEVEL q18e(NOMINAL).
VARIABLE LEVEL q18f(NOMINAL).
VARIABLE LEVEL q19a(NOMINAL).
VARIABLE LEVEL q19b(SCALE).
VARIABLE LEVEL q20(NOMINAL).
VARIABLE LEVEL q21(NOMINAL).
VARIABLE LEVEL q22a(SCALE).
VARIABLE LEVEL q22b(SCALE).
VARIABLE LEVEL q22c(SCALE).
VARIABLE LEVEL q22d(SCALE).
VARIABLE LEVEL q22e(SCALE).
VARIABLE LEVEL q23a(NOMINAL).
VARIABLE LEVEL q23b(NOMINAL).
VARIABLE LEVEL q23c(NOMINAL).
VARIABLE LEVEL q23d(NOMINAL).
VARIABLE LEVEL q24a(NOMINAL).
VARIABLE LEVEL q24b(NOMINAL).
VARIABLE LEVEL q24c(NOMINAL).
VARIABLE LEVEL q24d(NOMINAL).
VARIABLE LEVEL q25a(ORDINAL).
VARIABLE LEVEL q25b(ORDINAL).
VARIABLE LEVEL q25c(ORDINAL).
VARIABLE LEVEL q25d(ORDINAL).
VARIABLE LEVEL q25e(ORDINAL).
VARIABLE LEVEL q25f(ORDINAL).
VARIABLE LEVEL q25g(ORDINAL).
VARIABLE LEVEL q25h(ORDINAL).
VARIABLE LEVEL q25i(ORDINAL).
VARIABLE LEVEL q25j(ORDINAL).
VARIABLE LEVEL q25k(ORDINAL).
VARIABLE LEVEL q26a(ORDINAL).
VARIABLE LEVEL q26b(ORDINAL).
VARIABLE LEVEL q27a(NOMINAL).
VARIABLE LEVEL q27b(NOMINAL).
VARIABLE LEVEL q27c(NOMINAL).
VARIABLE LEVEL q27d(NOMINAL).
VARIABLE LEVEL q27e(NOMINAL).
VARIABLE LEVEL q27f(NOMINAL).
VARIABLE LEVEL q27g(NOMINAL).
VARIABLE LEVEL q27h(NOMINAL).
VARIABLE LEVEL q27i(NOMINAL).
VARIABLE LEVEL q27j(NOMINAL).
VARIABLE LEVEL q27k(NOMINAL).
VARIABLE LEVEL q27l(NOMINAL).
VARIABLE LEVEL q27m(NOMINAL).
VARIABLE LEVEL q28(NOMINAL).
VARIABLE LEVEL q28_mens(ORDINAL).
VARIABLE LEVEL q29a(NOMINAL).
VARIABLE LEVEL q29b(NOMINAL).
VARIABLE LEVEL q29c(NOMINAL).
VARIABLE LEVEL q29d(NOMINAL).
VARIABLE LEVEL q29e(NOMINAL).
VARIABLE LEVEL q29f(NOMINAL).
VARIABLE LEVEL q29g(NOMINAL).
VARIABLE LEVEL q29h(NOMINAL).
VARIABLE LEVEL q29i(NOMINAL).
VARIABLE LEVEL q29j(NOMINAL).
VARIABLE LEVEL q30(NOMINAL).
VARIABLE LEVEL q31(NOMINAL).
VARIABLE LEVEL q32(NOMINAL).
VARIABLE LEVEL q33(ORDINAL).
VARIABLE LEVEL q34(NOMINAL).
VARIABLE LEVEL q34a(SCALE).
VARIABLE LEVEL q34b(NOMINAL).
VARIABLE LEVEL q35(NOMINAL).
VARIABLE LEVEL q36(NOMINAL).
VARIABLE LEVEL q37a(NOMINAL).
VARIABLE LEVEL q37b(NOMINAL).
VARIABLE LEVEL q38a(NOMINAL).
VARIABLE LEVEL q38b(NOMINAL).
VARIABLE LEVEL q38c(NOMINAL).
VARIABLE LEVEL q38d(NOMINAL).
VARIABLE LEVEL q38e(NOMINAL).
VARIABLE LEVEL q38f(NOMINAL).
VARIABLE LEVEL q38g(NOMINAL).
VARIABLE LEVEL q39a(NOMINAL).
VARIABLE LEVEL q39b(NOMINAL).
VARIABLE LEVEL q39c(NOMINAL).
VARIABLE LEVEL q39d(NOMINAL).
VARIABLE LEVEL q39e(NOMINAL).
VARIABLE LEVEL q40a(NOMINAL).
VARIABLE LEVEL q40b(NOMINAL).
VARIABLE LEVEL q40c(NOMINAL).
VARIABLE LEVEL ie1(NOMINAL).
VARIABLE LEVEL ie3(NOMINAL).
VARIABLE LEVEL i1_age(SCALE).
VARIABLE LEVEL i2(NOMINAL).
VARIABLE LEVEL i3(NOMINAL).
VARIABLE LEVEL i4(NOMINAL).
VARIABLE LEVEL i4a(NOMINAL).
VARIABLE LEVEL i4b(NOMINAL).
VARIABLE LEVEL i4c(NOMINAL).
VARIABLE LEVEL i4d(NOMINAL).
VARIABLE LEVEL i4e(NOMINAL).
VARIABLE LEVEL i5a(SCALE).
VARIABLE LEVEL i5ba(NOMINAL).
VARIABLE LEVEL i5bb(NOMINAL).
VARIABLE LEVEL i5bc(NOMINAL).
VARIABLE LEVEL i5bd(NOMINAL).
VARIABLE LEVEL i5be(NOMINAL).
VARIABLE LEVEL i5bf(NOMINAL).
VARIABLE LEVEL i5bg(NOMINAL).
VARIABLE LEVEL i6a(NOMINAL).
VARIABLE LEVEL i6b(ORDINAL).
VARIABLE LEVEL i7(ORDINAL).
VARIABLE LEVEL i8(NOMINAL).
VARIABLE LEVEL i9(NOMINAL).
VARIABLE LEVEL i10_annee(SCALE).
VARIABLE LEVEL i10_comm(NOMINAL).
VARIABLE LEVEL i10_cant(NOMINAL).
VARIABLE LEVEL i11(NOMINAL).
VARIABLE LEVEL i12a(NOMINAL).
VARIABLE LEVEL i12b(NOMINAL).
VARIABLE LEVEL i12c(NOMINAL).
VARIABLE LEVEL i12d(NOMINAL).
VARIABLE LEVEL i13(NOMINAL).
VARIABLE LEVEL i14a(NOMINAL).
VARIABLE LEVEL i14b(NOMINAL).
VARIABLE LEVEL i14c(NOMINAL).
VARIABLE LEVEL i14d(NOMINAL).
VARIABLE LEVEL i14e(NOMINAL).
VARIABLE LEVEL i14f(NOMINAL).
VARIABLE LEVEL i15a(NOMINAL).
VARIABLE LEVEL i15b(NOMINAL).
VARIABLE LEVEL i15c(NOMINAL).
VARIABLE LEVEL i15d(NOMINAL).
VARIABLE LEVEL i15e(NOMINAL).
VARIABLE LEVEL i15f(NOMINAL).
VARIABLE LEVEL i15g(NOMINAL).
VARIABLE LEVEL i15h(NOMINAL).
VARIABLE LEVEL i15i(NOMINAL).
VARIABLE LEVEL i15j(NOMINAL).
VARIABLE LEVEL i15k(NOMINAL).
VARIABLE LEVEL i16(NOMINAL).
VARIABLE LEVEL i17a(ORDINAL).
VARIABLE LEVEL i17b(NOMINAL).
VARIABLE LEVEL i18a(NOMINAL).
VARIABLE LEVEL i18b(NOMINAL).
VARIABLE LEVEL i18c(NOMINAL).
VARIABLE LEVEL i18d(NOMINAL).
VARIABLE LEVEL i19(NOMINAL).
VARIABLE LEVEL i19a(NOMINAL).
VARIABLE LEVEL i19b(NOMINAL).
VARIABLE LEVEL i19c(NOMINAL).
VARIABLE LEVEL i19d(NOMINAL).
VARIABLE LEVEL i20(NOMINAL).
VARIABLE LEVEL i20a(NOMINAL).
VARIABLE LEVEL i20b(NOMINAL).
VARIABLE LEVEL i20c(NOMINAL).
VARIABLE LEVEL i21a(ORDINAL).
VARIABLE LEVEL i21b(ORDINAL).
VARIABLE LEVEL i21c(ORDINAL).
VARIABLE LEVEL i21d(ORDINAL).
VARIABLE LEVEL i21e(ORDINAL).
VARIABLE LEVEL i21f(ORDINAL).
VARIABLE LEVEL i22a(NOMINAL).
VARIABLE LEVEL i22b(NOMINAL).
VARIABLE LEVEL i23a(NOMINAL).
VARIABLE LEVEL i23b(NOMINAL).
VARIABLE LEVEL i23c(NOMINAL).
VARIABLE LEVEL i23d(NOMINAL).
VARIABLE LEVEL i24(NOMINAL).
VARIABLE LEVEL i24_age(SCALE).
VARIABLE LEVEL i25(NOMINAL).
VARIABLE LEVEL i25_age(SCALE).
VARIABLE LEVEL i26(NOMINAL).
VARIABLE LEVEL i26b(NOMINAL).
VARIABLE LEVEL i27a(NOMINAL).
VARIABLE LEVEL i27b(NOMINAL).
VARIABLE LEVEL i27c(NOMINAL).
VARIABLE LEVEL i27d(NOMINAL).
VARIABLE LEVEL i27e(NOMINAL).
VARIABLE LEVEL i28a(NOMINAL).
VARIABLE LEVEL i28b(NOMINAL).
VARIABLE LEVEL i28c(NOMINAL).
VARIABLE LEVEL i28d(NOMINAL).
VARIABLE LEVEL i28e(NOMINAL).
VARIABLE LEVEL i29a(NOMINAL).
VARIABLE LEVEL i29b(NOMINAL).
VARIABLE LEVEL i30a(ORDINAL).
VARIABLE LEVEL i30b(ORDINAL).
VARIABLE LEVEL i30c(ORDINAL).
VARIABLE LEVEL i30d(ORDINAL).
VARIABLE LEVEL i30e(ORDINAL).
VARIABLE LEVEL i30f(ORDINAL).
VARIABLE LEVEL i31a(ORDINAL).
VARIABLE LEVEL i31b(ORDINAL).
VARIABLE LEVEL i31c(ORDINAL).
VARIABLE LEVEL i31d(ORDINAL).
VARIABLE LEVEL i31e(ORDINAL).
VARIABLE LEVEL i31f(ORDINAL).
VARIABLE LEVEL i32(NOMINAL).
VARIABLE LEVEL i33(NOMINAL).
VARIABLE LEVEL i34_prin(NOMINAL).
VARIABLE LEVEL i34_seco(NOMINAL).
VARIABLE LEVEL i35(NOMINAL).
VARIABLE LEVEL i36(NOMINAL).
VARIABLE LEVEL i37a(ORDINAL).
VARIABLE LEVEL i37b(ORDINAL).
VARIABLE LEVEL i37c(ORDINAL).
VARIABLE LEVEL i37d(ORDINAL).
VARIABLE LEVEL i37e(ORDINAL).
VARIABLE LEVEL i37f(ORDINAL).
VARIABLE LEVEL i37g(ORDINAL).
VARIABLE LEVEL i37h(ORDINAL).
VARIABLE LEVEL i38(NOMINAL).
VARIABLE LEVEL i38a(NOMINAL).
VARIABLE LEVEL i38b(NOMINAL).
VARIABLE LEVEL i38c(NOMINAL).
VARIABLE LEVEL i38d(NOMINAL).
VARIABLE LEVEL i38e(NOMINAL).
VARIABLE LEVEL i38f(NOMINAL).
VARIABLE LEVEL i38g(NOMINAL).
VARIABLE LEVEL i38h(NOMINAL).
VARIABLE LEVEL i39(NOMINAL).
VARIABLE LEVEL i39_anne(SCALE).
VARIABLE LEVEL i39_repr(NOMINAL).
VARIABLE LEVEL i39_prof(NOMINAL).
VARIABLE LEVEL i39_pr_1(NOMINAL).
VARIABLE LEVEL i39_pr_2(NOMINAL).
VARIABLE LEVEL i39_pr_3(NOMINAL).
VARIABLE LEVEL i40(SCALE).
VARIABLE LEVEL i41(NOMINAL).
VARIABLE LEVEL i43(NOMINAL).
VARIABLE LEVEL i44a(NOMINAL).
VARIABLE LEVEL i44b(NOMINAL).
VARIABLE LEVEL i45(ORDINAL).
VARIABLE LEVEL i46(NOMINAL).
VARIABLE LEVEL i47a1(NOMINAL).
VARIABLE LEVEL i47a2(ORDINAL).
VARIABLE LEVEL i47a3(ORDINAL).
VARIABLE LEVEL i47a4(ORDINAL).
VARIABLE LEVEL i47a5(NOMINAL).
VARIABLE LEVEL i47a6(NOMINAL).
VARIABLE LEVEL i47b1(NOMINAL).
VARIABLE LEVEL i47b2(ORDINAL).
VARIABLE LEVEL i47b3(ORDINAL).
VARIABLE LEVEL i47b4(ORDINAL).
VARIABLE LEVEL i47b5(NOMINAL).
VARIABLE LEVEL i47b6(NOMINAL).
VARIABLE LEVEL i47c1(NOMINAL).
VARIABLE LEVEL i47c2(ORDINAL).
VARIABLE LEVEL i47c3(ORDINAL).
VARIABLE LEVEL i47c4(ORDINAL).
VARIABLE LEVEL i47c5(NOMINAL).
VARIABLE LEVEL i47c6(NOMINAL).
VARIABLE LEVEL i47d1(NOMINAL).
VARIABLE LEVEL i47d2(ORDINAL).
VARIABLE LEVEL i47d3(ORDINAL).
VARIABLE LEVEL i47d4(ORDINAL).
VARIABLE LEVEL i47d5(NOMINAL).
VARIABLE LEVEL i47d6(NOMINAL).
VARIABLE LEVEL i47e1(NOMINAL).
VARIABLE LEVEL i47e2(ORDINAL).
VARIABLE LEVEL i47e3(ORDINAL).
VARIABLE LEVEL i47e4(ORDINAL).
VARIABLE LEVEL i47e5(NOMINAL).
VARIABLE LEVEL i47e6(NOMINAL).
VARIABLE LEVEL i47f1(NOMINAL).
VARIABLE LEVEL i47f2(ORDINAL).
VARIABLE LEVEL i47f3(ORDINAL).
VARIABLE LEVEL i47f4(ORDINAL).
VARIABLE LEVEL i47f5(NOMINAL).
VARIABLE LEVEL i47f6(NOMINAL).
VARIABLE LEVEL i47g1(NOMINAL).
VARIABLE LEVEL i47g2(ORDINAL).
VARIABLE LEVEL i47g3(ORDINAL).
VARIABLE LEVEL i47g4(ORDINAL).
VARIABLE LEVEL i47g5(NOMINAL).
VARIABLE LEVEL i47g6(NOMINAL).
VARIABLE LEVEL i47h1(NOMINAL).
VARIABLE LEVEL i47h2(ORDINAL).
VARIABLE LEVEL i47h3(ORDINAL).
VARIABLE LEVEL i47h4(ORDINAL).
VARIABLE LEVEL i47h5(NOMINAL).
VARIABLE LEVEL i47h6(NOMINAL).
VARIABLE LEVEL i47i1(NOMINAL).
VARIABLE LEVEL i47i2(ORDINAL).
VARIABLE LEVEL i47i3(ORDINAL).
VARIABLE LEVEL i47i4(ORDINAL).
VARIABLE LEVEL i47i5(NOMINAL).
VARIABLE LEVEL i47i6(NOMINAL).
VARIABLE LEVEL i47j1(NOMINAL).
VARIABLE LEVEL i47j2(ORDINAL).
VARIABLE LEVEL i47j3(ORDINAL).
VARIABLE LEVEL i47j4(ORDINAL).
VARIABLE LEVEL i47j5(NOMINAL).
VARIABLE LEVEL i47j6(NOMINAL).
VARIABLE LEVEL i47k1(NOMINAL).
VARIABLE LEVEL i47k2(ORDINAL).
VARIABLE LEVEL i47k3(ORDINAL).
VARIABLE LEVEL i47k4(ORDINAL).
VARIABLE LEVEL i47k5(NOMINAL).
VARIABLE LEVEL i47k6(NOMINAL).
VARIABLE LEVEL i51f(NOMINAL).
VARIABLE LEVEL i52a(NOMINAL).
VARIABLE LEVEL i52b(NOMINAL).
VARIABLE LEVEL i52c(NOMINAL).
VARIABLE LEVEL i52d(NOMINAL).
VARIABLE LEVEL i52e(NOMINAL).
VARIABLE LEVEL i52f(NOMINAL).
VARIABLE LEVEL i53(NOMINAL).
VARIABLE LEVEL i53_type(NOMINAL).
VARIABLE LEVEL i54a(NOMINAL).
VARIABLE LEVEL i54b(NOMINAL).
VARIABLE LEVEL i54c(NOMINAL).
VARIABLE LEVEL i54d(NOMINAL).
VARIABLE LEVEL i54e(NOMINAL).
VARIABLE LEVEL i55a(NOMINAL).
VARIABLE LEVEL i55b(NOMINAL).
VARIABLE LEVEL i55c(NOMINAL).
VARIABLE LEVEL i55d(NOMINAL).
VARIABLE LEVEL i55e(NOMINAL).
VARIABLE LEVEL i55f(NOMINAL).
VARIABLE LEVEL i55g(NOMINAL).
VARIABLE LEVEL i56a(NOMINAL).
VARIABLE LEVEL i56b(NOMINAL).
VARIABLE LEVEL i56c(NOMINAL).
VARIABLE LEVEL i56d(NOMINAL).
VARIABLE LEVEL i56e(NOMINAL).
VARIABLE LEVEL i56f(NOMINAL).
VARIABLE LEVEL i56g(NOMINAL).
VARIABLE LEVEL i57a(NOMINAL).
VARIABLE LEVEL i57b(NOMINAL).
VARIABLE LEVEL i57c(NOMINAL).
VARIABLE LEVEL i57d(NOMINAL).
VARIABLE LEVEL i57e(NOMINAL).
VARIABLE LEVEL i58(ORDINAL).
VARIABLE LEVEL i59(NOMINAL).
VARIABLE LEVEL i60(NOMINAL).
VARIABLE LEVEL i61(NOMINAL).
VARIABLE LEVEL i62ac_01(NOMINAL).
VARIABLE LEVEL i62av_01(NOMINAL).
VARIABLE LEVEL i62co_01(NOMINAL).
VARIABLE LEVEL i62ac_02(NOMINAL).
VARIABLE LEVEL i62av_02(NOMINAL).
VARIABLE LEVEL i62co_02(NOMINAL).
VARIABLE LEVEL i62ac_03(NOMINAL).
VARIABLE LEVEL i62av_03(NOMINAL).
VARIABLE LEVEL i62co_03(NOMINAL).
VARIABLE LEVEL i62ac_04(NOMINAL).
VARIABLE LEVEL i62av_04(NOMINAL).
VARIABLE LEVEL i62co_04(NOMINAL).
VARIABLE LEVEL i62ac_05(NOMINAL).
VARIABLE LEVEL i62av_05(NOMINAL).
VARIABLE LEVEL i62co_05(NOMINAL).
VARIABLE LEVEL i62ac_06(NOMINAL).
VARIABLE LEVEL i62av_06(NOMINAL).
VARIABLE LEVEL i62co_06(NOMINAL).
VARIABLE LEVEL i62ac_07(NOMINAL).
VARIABLE LEVEL i62av_07(NOMINAL).
VARIABLE LEVEL i62co_07(NOMINAL).
VARIABLE LEVEL i62ac_08(NOMINAL).
VARIABLE LEVEL i62av_08(NOMINAL).
VARIABLE LEVEL i62co_08(NOMINAL).
VARIABLE LEVEL i62ac_09(NOMINAL).
VARIABLE LEVEL i62av_09(NOMINAL).
VARIABLE LEVEL i62co_09(NOMINAL).
VARIABLE LEVEL i62ac_10(NOMINAL).
VARIABLE LEVEL i62av_10(NOMINAL).
VARIABLE LEVEL i62co_10(NOMINAL).
VARIABLE LEVEL i62ac_11(NOMINAL).
VARIABLE LEVEL i62av_11(NOMINAL).
VARIABLE LEVEL i62co_11(NOMINAL).
VARIABLE LEVEL i62ac_12(NOMINAL).
VARIABLE LEVEL i62av_12(NOMINAL).
VARIABLE LEVEL i62co_12(NOMINAL).
VARIABLE LEVEL i62ac_13(NOMINAL).
VARIABLE LEVEL i62av_13(NOMINAL).
VARIABLE LEVEL i62co_13(NOMINAL).
VARIABLE LEVEL i62ac_14(NOMINAL).
VARIABLE LEVEL i62av_14(NOMINAL).
VARIABLE LEVEL i62co_14(NOMINAL).
VARIABLE LEVEL i62ac_15(NOMINAL).
VARIABLE LEVEL i62av_15(NOMINAL).
VARIABLE LEVEL i62co_15(NOMINAL).
VARIABLE LEVEL i62ac_16(NOMINAL).
VARIABLE LEVEL i62av_16(NOMINAL).
VARIABLE LEVEL i62co_16(NOMINAL).
VARIABLE LEVEL i62ac_17(NOMINAL).
VARIABLE LEVEL i62av_17(NOMINAL).
VARIABLE LEVEL i62co_17(NOMINAL).
VARIABLE LEVEL i62radio(NOMINAL).
VARIABLE LEVEL i63_1(ORDINAL).
VARIABLE LEVEL i63_2(ORDINAL).
VARIABLE LEVEL i63_3(ORDINAL).
VARIABLE LEVEL i63_4(ORDINAL).
VARIABLE LEVEL i63_5(ORDINAL).
VARIABLE LEVEL i63_6(ORDINAL).
VARIABLE LEVEL i63_7(ORDINAL).
VARIABLE LEVEL i63_8(ORDINAL).
VARIABLE LEVEL i63_9(ORDINAL).
VARIABLE LEVEL i63_10(ORDINAL).
VARIABLE LEVEL i63_11(ORDINAL).
VARIABLE LEVEL i63_12(ORDINAL).
VARIABLE LEVEL i63_13(ORDINAL).
VARIABLE LEVEL i63_14(ORDINAL).
VARIABLE LEVEL i64a(NOMINAL).
VARIABLE LEVEL i64b(NOMINAL).
VARIABLE LEVEL i65_1(NOMINAL).
VARIABLE LEVEL i65_2(NOMINAL).
VARIABLE LEVEL i65_3(NOMINAL).
VARIABLE LEVEL i65_4(NOMINAL).
VARIABLE LEVEL i65_5(NOMINAL).
VARIABLE LEVEL i65_6(NOMINAL).
VARIABLE LEVEL i65_7(NOMINAL).
VARIABLE LEVEL i65_8(NOMINAL).
VARIABLE LEVEL i65_9(NOMINAL).
VARIABLE LEVEL i65_10(NOMINAL).
VARIABLE LEVEL i65_11(NOMINAL).
VARIABLE LEVEL i65_12(NOMINAL).
VARIABLE LEVEL i65_13(NOMINAL).
VARIABLE LEVEL i65_14(NOMINAL).
VARIABLE LEVEL i65_15(NOMINAL).
VARIABLE LEVEL i65_16(NOMINAL).
VARIABLE LEVEL i65_17(NOMINAL).
VARIABLE LEVEL i65_18(NOMINAL).
VARIABLE LEVEL i65_19(NOMINAL).
VARIABLE LEVEL i65_20(NOMINAL).
VARIABLE LEVEL i65_21(NOMINAL).
VARIABLE LEVEL i65_22(NOMINAL).
VARIABLE LEVEL i66a(NOMINAL).
VARIABLE LEVEL i66b(NOMINAL).
VARIABLE LEVEL i66c(NOMINAL).
VARIABLE LEVEL i66d(NOMINAL).
VARIABLE LEVEL i66e(NOMINAL).
VARIABLE LEVEL i66f(NOMINAL).
VARIABLE LEVEL i66g(NOMINAL).
VARIABLE LEVEL i67(NOMINAL).
VARIABLE LEVEL i68(NOMINAL).
VARIABLE LEVEL i69a1(NOMINAL).
VARIABLE LEVEL i69a2(NOMINAL).
VARIABLE LEVEL i69b1(ORDINAL).
VARIABLE LEVEL i69b2(ORDINAL).
VARIABLE LEVEL i69_nomb(NOMINAL).
VARIABLE LEVEL i70(NOMINAL).
VARIABLE LEVEL i71(NOMINAL).
VARIABLE LEVEL i72a(NOMINAL).
VARIABLE LEVEL i72b(NOMINAL).
VARIABLE LEVEL i72c(NOMINAL).
VARIABLE LEVEL i72d_enf(NOMINAL).
VARIABLE LEVEL i72d_aut(NOMINAL).
VARIABLE LEVEL i72e(NOMINAL).
VARIABLE LEVEL i72f(NOMINAL).
VARIABLE LEVEL i72g(NOMINAL).
VARIABLE LEVEL i73(NOMINAL).
VARIABLE LEVEL i74(ORDINAL).
VARIABLE LEVEL i75(ORDINAL).
VARIABLE LEVEL i76(NOMINAL).
VARIABLE LEVEL i77(NOMINAL).
VARIABLE LEVEL i78(NOMINAL).
VARIABLE LEVEL i79(ORDINAL).
VARIABLE LEVEL i80(NOMINAL).
VARIABLE LEVEL i80_age(SCALE).
VARIABLE LEVEL i80b(NOMINAL).
VARIABLE LEVEL i80b_age(SCALE).
VARIABLE LEVEL i81(NOMINAL).
VARIABLE LEVEL i81_age(SCALE).
VARIABLE LEVEL i81b(NOMINAL).
VARIABLE LEVEL i81b_age(SCALE).
VARIABLE LEVEL e1(NOMINAL).
VARIABLE LEVEL e2(ORDINAL).
VARIABLE LEVEL e3(NOMINAL).
VARIABLE LEVEL e4a(NOMINAL).
VARIABLE LEVEL e4b(NOMINAL).
VARIABLE LEVEL e4c(NOMINAL).
VARIABLE LEVEL e5(NOMINAL).

* Définition des missings

MISSING VALUES v1 TO e5 (LOWEST THRU -1).
