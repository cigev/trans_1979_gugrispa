# Mise à l'écart et dépendance des personnes âgées

«Mise à l'écart et dépendance des personnes âgées» est une enquête transversale sur la santé des personnes âgées en Valais et à Genève, réalisée en 1978-1979 par le GUGRISPA (Groupe universitaire genevois de recherche interdisciplinaire sur les personnes âgées), ancêtre du CIGEV qui dépendait à l'époque de l'Institut de médecine sociale et préventive.

La recherche porte sur des facteurs (internes et environnementaux) et des processus qui favorisent ou soutiennent l'autonomie de la personne vieillisante, son intégrité physique et psychique et sa participation à la vie en société, ou au contraire qui entravent ou portent atteinte à celles-ci. Cet objectif est poursuivi au moyen d'une enquête transversale auprès d'un échantillon représentatif de la population âgée de 62 ans et plus de deux régions typées (canton de Genève et Valais central) en 1978-1979. Le choix des deux régions d'enquête s'explique par le fait qu'elles constituent typologiquement les deux extrêmes de la «fourchette» représentant la diversité des situations des personnes âgées en Suisse. Cette enquête transversale sera répétée en 1994 et complétée par une enquête longitudinale (SWILSOO) sur la cohorte des 80-84 ans vivant à domicile au moment du passage de l'enquête transversale de 1994. Une troisième enquête transversale sera conduite durant les années 2011-2012.

La recherche s'inscrit dans la perspective théorique du parcours de vie (life course). À la stratification sociale «horizontale» (de classes) s'articule une stratification «verticale», une organisation sociale du temps de vie, définissant étapes et transitions. L'hypothèse est que la position occupée dans le parcours de vie est un meilleur indicateur que l'âge tant pour définir des situations et problèmes-types que par rapport aux pratiques des personnes vieillissantes. L'idée est donc de construire et de valider un indicateur de position dans le parcours de vie (IPPV) combinant la position individuelle dans trois dimensions: le parcours de santé, la trajectoire familiale, la trajectoire professionnelle.

Par les résultats de la recherche, l'équipe de recherche a voulu contribuer à une réflexion sur les fondements éthiques de la recherche en gérontologie et d'une politique de la vieillesse, à la définition des besoins présents et à venir des personnes vieillissantes, et à la mise en lumière des possibles articulations entre le soutien que peuvent apporter la famille ou les réseaux d'entraide, et les diverses formes d'aides institutionnelles.

## Citation

(FR)
> Lalive d'Épinay, C. (dir.). (1979). *Mise à l'écart et dépendance des personnes âgées* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Lalive d'Épinay, C. (Dir.). (1979). *Mise à l'écart et dépendance des personnes âgées* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

## Auteurs

* **Christian Lalive d'Épinay** – *Chef de projet*
* **Étienne Christe** – *Chercheur associé*
* **Josette Coenen-Huter** – *Chercheuse associée*
* **Hermann-Michel Hagmann** – *Co-requérant*
* **Olivier Jeanneret** – *Premier co-requérant*
* **Jean-Pierre Junod** – *Co-requérant*
* **Jean Kellerhals** – *Co-requérant*
* **Luc Raymond** – *Co-requérant*
* **Jean-Pierre Schellhorn** – *Co-requérant*
* **Geneviève Wirth** – *Chercheuse associée*
* **Bernard de Wurstenberger** – *Chercheur associé*

### Collaboratrices et collaborateurs

* **Alain Clémence**
* **Marianne Modak**
* **Marie-Noëlle La Spada-Schurmans**
* **Catherine Borrini**
* **Claudio Bolzman**
* **France von Allmen**
* **Dominique Froidevaux**
* **Isabelle Hirschi**
* **Emmanuel Lazega**

## Financement

Fonds national suisse de la recherche scientifique (FNS), Programme national de recherche no 3 ([PNR 3](http://www.snf.ch/fr/pointrecherche/programmes-nationaux-de-recherche/pnr03-problemes-dintegration-sociale-en-suisse/Pages/default.aspx)): «Problèmes d'intégration sociale en Suisse: les jeunes; les personnes âgées».

## Thèmes

Interviewé: commune, sexe, date de naissance, état civil, confession, lieu où l'interviewé a passé son enfance, année du: mariage, veuvage, divorce, remariage; nombre d'enfants et de petits-enfants.

Enfant(s) de l'interviewé: pour chaque enfant: sexe, année de naissance, année du décès, activité principale, activité principale du conjoint, lieu de résidence, nombre d'enfant(s).

Famille de l'interviewé: nombre de frères et soeurs à l'âge de 20 ans, nombre de frères et soeurs vivant encore aujourd'hui; rang de l'interviewé dans la fratrie, nombre de (petits-)neveux et (petites-)nièces âgés de moins de 15 ans.

Conjoint: année de naissance, situation professionnelle à l'âge de 45 ans, pour les femmes mariées: année de la retraite du mari, changements positifs/négatifs liés à la retraite du mari.

Réseau familial: existence d'au moins une personne de la famille de l'interviewé qui: lui demande son avis, aime passer de longs moments avec lui/elle, a une affection profonde pour lui/elle, etc.

Réseau social: type de relation entretenu avec les voisins, existance d'amis intimes, résidence et fréquence de contact avec les deux amis les plus proches; situation actuelle comparée à celle à 45 ans: fréquence des visites de la famille et des amis.

Vie en société: type(s) d'association(s) dont l'interviewé fait partie, nombre de réunions par mois auxquelles l'interviewé participe; service rendu aux enfants: faire le ménage, préparer des repas, garder leurs enfants, etc.; aide ou soins apportés par l'interviewé à une personne de son entourage: bénéficiaire; possession d'une voiture, fréquence d'utilisation de la voiture; fréquence et pratique actuelle de diverses activités: promenades, aller au café, faire des mots-croisés etc.; fréquence et comparaison avec pratique à 45 ans; participation aux votations.

Assurances: assurance(s) que l'interviewé/le conjoint a souscrite(s): assurance-maladie, assurance-accident, assurance-vie, etc.

État actuel: appétit, sentiment d'isolement, problèmes de dormir, etc.; problèmes de mémoire/de motivation; degré de difficulté à accomplir certaines activités: se déplacer, monter un escalier, se coucher et se lever, etc.; fréquence et type des services obtenus d'un membre de la famille, des voisins, d'un service social; tâches partagées ou accomplies seul: préparer les repas, faire les achats, faire les grands nettoyages, etc.

Santé: entre 45 ans et maintenant: arrêt(s) de travail de plus de deux mois pour raison de santé, âge à cette interruption, cause, conséquences au niveau de la santé, au niveau financier; propre état de santé actuel: évaluation, comparaison avec celui des personnes du même âge, fréquence et gravité des troubles: différents types de douleurs, troubles de l'ouie, palpitations, etc.: type de soin pour ces maux: consulter le médecin, traitement médical; maladie/suites d'un accident ou d'une opération dont l'interviewé souffre actuellement et âge auquel cette maladie/cet accident/cette opération est survenu/e; au cours des 12 derniers mois visites chez le: médecin, dentiste, physiothérapeute, etc.; personne qui s'occuperait principalement de l'interviewé lors d'une éventuelle maladie, qui nécessite des soins et dont l'interviewé s'occupe régulièrement.

Informations sur la vie passée: profession exercée par le père durant la jeunesse de l'interviewé, dernière école fréquentée, année du premier emploi régulier; cotisation à une caisse de retraite; situation à 45 ans: commune de résidence, statut professionnel, profession exercée, taux et secteur d'activité, nombre de subordonnés, membre d'association, évaluation de l'état de santé à 45 ans, troubles durables ou chroniques, visites régulières chez le médecin/dentiste, habitude de fumer comparée avec la pratique actuelle; situation entre 45 et 50 ans: changements professionnels; situation à 60 ans: profession exercée et statut professionnel, taux et secteur d'activité, nombre de subordonnés, jugement global sur ce travail; retraite: à l'âge légal, repoussée ou anticipée; raisons à la base de: la poursuite de l'activité professionnelle au-delà de l'âge légal, la retraite anticipée; si poursite d'une activité rémunérée: type, taux et raisons; manque ressenti depuis la retraite: la compagnie, le sentiment d'être utile, les rentrées d'argent, etc.

Veuves et veufs: suite au décès du conjoint degré de gravité de divers problèmes: problèmes financiers, manque de contacts sociaux, solitude, etc.; personnes ayant aidé à faire face à ces problèmes.

Logement: chez soi (propriétaire/locataire), équipement du logement: eau chaude, WC dans l'appartement, téléphone, etc.; nombre de personnes vivant dans le même logement que l'interviewé; si chez un de ses enfants/des parent(s)/des ami(e)s: dans ce logemement depuis combien de temps; répartition des frais de logement; personnes vivant seules: souhait de partager le logement et personnes avec lesquelles l'interviewé aimerait vivre; années de résidence dans: le logement actuel, la commune actuelle (en ville: le quartier); si moins de 15 ans de résidence dans l'actuelle commune/l'actuel quartier de domicile: lieu de résidence précédent, raisons ayant motivé le déménagement; nombre de pièces; durée du trajet à pied entre le domicile et: le magasin d'alimentation dans lequel l'interviewé se sert habituellement, l'arrêt de transport public le plus proche, le café ou le restaurant que l'interviewé fréquente le plus souvent.

Médias: fréquence à laquelle l'interviewé: écoute la radio, regarde la télévision, lit un journal, lit des livres ou des revues.

Situation financière: revenu individuel, fortune, source(s) du revenu: salaire, retraite, intérêts d'épargne, revenu d'une location, etc.; pourcentage du revenu actuel par rapport au revenu avant la retraite.

Opinions: utilité des gens âgés dans notre société, importance de faire attention à son apparence, comprendre les besoins des enfants qui mènent leur vie, etc. Événements survenus dans le monde/en Suisse qui ont le plus marqué l'interviewé et pour quelle raison.

Intervieweur: habitation de l'interviewé: type, lieu de résidence, étage, nombre de logements dans le bâtiment; accès à l'habitation: escalier, ascenceur; évaluation de l'interviewé: problèmes de mémoire, difficultés d'audition, difficultés d'élocution, autre problème de santé, attitude de l'interviewé pendant l'interview; présence d'un tiers pendant à l'entretien, intervention du tiers, influence de l'intervention.

## Univers de référence

Population suisse de 65 ans et plus pour les hommes et de 62 ans et plus pour les femmes.

## Échantillonnage

Enquête transversale réalisée auprès d'un échantillon représentatif de la population âgée de 62, respectivement 65 ans et plus, stratifié par âge, sexe et région (canton de Genève et Valais central).

Pour la région «Genève», la population provient (après tirage aléatoire) de tout le canton de Genève, pour la région «Valais central», la population provient (après tirage aléatoire également) des quatre districts de Conthey, Hérens, Sion et Sierre.

Les non-réponses et les refus ont été remplacés par la méthode «des jumeaux» (remplacement d'une personne de l'échantillon original qui se révèle impossible à joindre par une autre, présentant les mêmes caractéristiques connues sur la base des données des registres de population).

Participation: échantillon de 1608 individus, dont 804 à Genève et 804 en Valais central.

## Récolte des données

Instrument: questionnaire. Une moitié du questionnaire a été remplie par la personne âgée seule, l'autre l'a été au cours d'une entrevue avec un·e enquêteur/trice.

Les enquêteurs/trices ont été mandatés et formés par le GUGRISPA.

Date du relevé: entre 1978 et 1980.

## Métadonnées

* Relevé effectué par: Université de Genève, GUGRISPA.
* Documentation: questionnaires, codebook, rapport final.
* Cas: 1608.
* Variables: 575.
* Format: CSV + syntaxe SPS pour importation dans SPSS.

## Licence

[CC-BY-SA (Licence Creative Commons Attribution - Partage dans les Mêmes Conditions 4.0 International)](LICENSE)

## Rapport de traitement

### Réalisation

* [SIDOS](http://forscenter.ch/) – 2005
* [CIGEV](https://cigev.unige.ch/) (Grégoire Métral) – 2017

### Mini-contrôle

#### Description du projet et de la méthode
La description du projet de recherche et de la méthode se trouvent dans le rapport final (voir la première référence des [publications](#publications) ci-dessous).

#### Questionnaires
Le questionnaire est subdivisé en deux fascicules (auto-administré et entretien face-à-face) et se trouve dans le fichier PDF ci-joint. Quatre variantes du questionnaire, de plus en plus allégées, ont également été utilisées pour tenir compte des situations variées que l'on rencontre lorsque l'on a affaire à des personnes âgées. Malheureusement, ces variantes ne sont plus disponibles (voir le rapport final p. 82-83).

#### Instructions aux enquêteurs/trices
Dans le questionnaire administré par l'enquêteur il y a une référence aux «Instructions générales». Malheureusement on ne dispose plus de ce document.

#### Matériel annexe
Dans le questionnaire administré par l'enquêteur on a utilisé pour la question 62 une carte avec une liste d'activités. Pour la question sur le revenu mensuel, question 71, il y a une carte avec des catégories. Malheureusement ces cartes ne sont plus à notre disposition. Pour le cas de la première, ce n'est pas grave, car elle doit ressembler au schéma du questionnaire. Mais pour les catégories du revenu c'est plus grave, car les labels de valeurs de cette variable ne sont pas définis dans le jeu de données.

#### Évaluation de la documentation
La documentation est suffisante même si l'information sur la différence entre les quatre versions du questionnaire est manquante.

#### Évaluation du ou des fichier(s) de données
Le jeu de données fourni pas les chercheurs est une construction de fichiers différents de plusieurs groupes de travail. Les mêmes variables y sont présentes sous differents noms, c'est-à-dire en plusieurs versions. Le SIDOS a préparé le jeu de données dans la mesure où il ne contient qu'une seule version par variable. Le nom correspond à la logique des questionnaires. Les chercheurs ont fourni après coup d'une part un jeu de données plus grand avec 88 cas de plus, qui inclut aussi les réponses des personnes en institution (celles qui ont répondu à un questionnaire plus court), et d'autre part un jeu de données avec des réponses à une question à multi-items qui manquait dans le jeu de données original (i47). Nous avons inséré les cas supplémentaires et les variables supplémentaires dans le jeu de données archivé.

Étant donné l'âge des données et leur transmission (saisie sur papier, codage sur cartes perforées, conversions successives, dernière transmission sous forme de fichier SPSS), l'état est bon.

### Variables

Le SIDOS a reçu une matrice des données qui englobe les variables des différents groupes de travail. Ces variables avaient des noms différents selon le nom du groupe de travail. La plupart des variables étaient les mêmes pour chaque bloc – sous des noms différents. Le SIDOS a vidé le jeu de données de toutes les différentes variantes et il a donné pour chaque variable un nouveau nom correspondant au code du questionnaire.

#### Explication des noms de variables
* v = variables descriptives: numéro, type du questionnaire, etc.
* q = variables du questionnaire rempli par la personne (auto-administré)
* ie = variables des questions à l'enquêteur au début du questionnaire face-à-face
* i = variables du questionnaire face-à-face
* e = questions à l'enquêteur à la fin du questionnaire face-à-face

Les numéros correspondent aux numéros du questionnaire.

Dans les cas où, dans le questionnaire, il y a deux sortes de sous questions pour la même question, on a ajouté des a, b, c... Tous les noms de variables ont été corrigés par des noms sans caractères accentués ni points (remplacés par des underscores).

#### Présence des labels de variables
Avec la syntaxe fournie, tous les labels de variables sont indiqués sous forme du libellé de la question, repris du questionnaire.

#### Variables construites et corrections
Catégories socioprofessionnelles: le choix des catégories et leur numération est expliqué dans
l'annexe 2 au préambule no 1, pages 92ff du Rapport final. En outre, les catégories socioprofessionnelles et leur codification en 11 classes sont présentées dans "Grille Gugrispa". Cette grille est intégrée dans la présente documentation pdf.

cspmen6 et cspind6: Ces deux variables à la fin du jeu de données classent les réponses de la profession du chef du ménage, respectivement de l'enquêté(e) selon les six catégories utilisées dans l'enquête de 1994.

q5 et i36: La question de l'état civil est posée dans les deux questionnaires (auto-administré et face-à-face). La variable du questionnaire administré par l'enquêteur (i36) ne correspond probablement pas à cette question, car selon un contrôle d'une table croisée les réponses diffèrent trop entre les deux questions. D'après les informations des chercheurs, la variable i36, qui est d'ailleurs nommé «état civil à 45 ans» dans le jeu de données – même si la question était l'état civil actuel est vraisemblablement une variable construite à partir du croisement des variables i36 et q6v1, q6v2, q6d1, q6d2, q6r1, q6r2, q4 et q4_regro.

Ni la construction de la variable q4 ("domicile enfance"), ni celle de la variable q4_regroupé ("commune enfance") ne sont documentées. Les codes des valeurs ne sont pas documentés. La variable q4_regroupé représente selon les chercheurs, éventuellement le suivant: «vraisemblablement, les personnes ayant cité un village en q4 ont été rattachées à leur commune en q4_regro. C'est donc la variable corrigée de q4». Malheureusement les chercheurs ne disposent plus de la liste des communes de q1 qui est probablement la base pour la construction de q4_regro.

Les grilles de codage sont manquantes pour les variables suivantes: q1, i10, i69a1, i69a2, q37a, q37b, i4e, i5b, i5j, i5k, i17b, i27e, i28e

Pour q37 et i10 on a aussi un autre problème: dans le questionnaire on parle d'abord de commune, puis de canton. Dans le jeu de données, les variables originales s'appelaient "domicile" (valeurs 0, 1, 2, ..., 18, 99), respectivement "commune" (valeurs 0, 1, 4, ..., 96, 99). On ne sait plus selon quel lien les variables correspondent aux questions et comment les réponses étaient codées.

Il n'y a pas de documentation pour les réponses «autre: ...».

La variable i17b ("genre de troubles de santé") est une question ouverte. Les réponses écrites ne sont pas documentées. On ne sait pas à quoi correspondent les valeurs de 1 à 19.

Toutes les années, récoltées sur 2 chiffres, ont été recodées en années à 4 chiffres. Cela a permis de détecter quelques erreurs ou incohérences qui ont été corrigées ou signalées. Il semble probable que dans certains cas, ce soit l'âge plutôt que l'année qui a été donné en réponse à une question. Exceptiohn: la question q34a, qui devrait être recodée en année à 4 chiffres, n'a pas été modifiée: il doit y avoir un mélange entre des années (à laquelle la voiture a été abandonnée) et des âges (de renoncement à la voiture) – les valeurs vont de 23 à 94 (pour un questionnaire rempli en 1978).

La question ie1 contenait les valeurs suivantes: 0, 1, 2, 3, 4, 5, 7, 8, 9 (alors que selon le questionnaire papier, seules les valeurs 1 à 5 devraient être présentes). Si les valeurs 0 ou 9 ont souvent été utilisées pour l'absence de réponse, il est possible que les valeurs 7, 8 et 9 aient été utilisées ici comme des missings divers (les cas concernés par ces valeurs ont beaucoup de missings sur toutes les autres questions). Nous avons recodé toutes les valeurs 0, 7, 8 et 9 en missing (-9).

Dans la batterie i5ba-i5bg, la colonne i5ba contenait beaucoup de 0 (ce qui n'était pas le cas des autres colonnes). Après examen détaillé de ces valeurs, nous avons recodé tous les 0 en 2 (ne vit pas avec le conjoint) ou en -7 (si la personne n'est pas mariée). De même, le code 2 a été recodé en -7 dans la variable i5bc pour les personnes qui n'ont pas d'enfants.

Question i40: nous avons recodé les 00 en -3 (sans objet) et les 01 en -4 (pas encore prise).

Variable i59: comme il n'y a aucune donnée manquante, il est possible que les 0 regroupent à la fois des personnes qui ont répondu "jamais" et des personnes qui n'ont pas répondu.

Question e2: Certains enquêteurs n'ayant visiblement pas répondu à la première question (code 0 trouvé dans le jeu de données SIDOS) ont répondu à la deuxième avec un étage différent de 0. On ne peut donc pas faire de différence, pour les code 0 de la variable e2, entre une absence de réponse ou un rez-de-chaussée. Nous avons donc conservé les 0 dans ce cas (et ne les avons donc pas recodés en -9 comme presque partout ailleurs).

De nombreuses autres corrections pour des individus particuliers ont été appliquées si les données étaient manifestements erronnées. En l'absence des questionnaires sur papier, il est impossible de vérifier plus avant l'origine de ces erreurs.

#### Variables contextuelles
Les variables du début de la matrice: v1 (numéro), v2 (échantillon), v3 (type questionnaire) et v4 (région).

#### Variable(s) de pondération
Aucune.

#### Variables disparues
Les réponses des questions ouvertes ne sont pas documentés (i42, i43a). Les réponses aux questions sur l'état de santé sont absentes (i48–i51). On a pu retrouver la question i47. Les questions à l'intervieweur au début et à la fin du questionnaire ne sont pas toutes intégrées. Il manque: ie2, e6, e7, e8, e9.

Pour toutes ces variables, il semble que la saisie n'a pas eu lieu en 1979. Aucun codebook de l'époque, ni base annexe retrouvée, ne contiennent ces variables.

### Valeurs

Tous les labels de valeurs ont été insérés selon le questionnaire.

Les filtres («si oui, passer à la question xxx») ont été pris en compte et les INAP ont été recodés.

Les codes sauvages constatés ont été mis en missing (voir ci-dessous).

Pour les mesures (scale, ordinal, nominal): le type a été indiqué systématiquement.

Quelques tests de plausibilité ont été effectués, notamment sur les années, mais pas systématiquement. L'absence des questionnaires ne permet pas d'approfondir davantage le travail.

#### Missing
Les non réponses ou données manquantes n'étaient pas documentées. Nous les avons systématiquement indiquées avec des valeurs négatives selon la logique suivante:

* -1 = Ne sait pas (NSP)
* -2 = Non réponse (NR)
* -7 = Inapproprié (INAP), en raison d'un filtre
* -9 = Valeur manquante non documentée (Missing)

D'autres valeurs négatives sont présentes lorsque la valeur indiquée était manifestement erronnée.

La syntaxe fournie permet de fixer en "missing" toutes les valeurs négatives.

## Codebook

[trans_1979_codebook.ods](trans_1979_codebook.ods)

## Publications

* Lalive d'Épinay, Christian; Christe, Étienne; Coenen-Huther, Josette; Hagmann, Hermann-Michel; Jeanneret, Olivier; Junod, Jean-Pierre; Kellerhals, Jean; Raymond, Luc; Schellhorn, Jean-Pierre; Wirth, Geneviève; de Wurstenberger, Bernard. (1982). *Mise à l'écart et dépendance des personnes âgées: Analyse des processus et des réponses. (Rapport final)*. Genève: GUGRISPA. 2 volumes.
* Lalive d'Epinay, Christian; Bickel, Jean-François; Maystre, Carole; Riand, Jean-François; Vollenwyder, Nathalie. (1997). Les personnes âgées à Genève, 1979-1994. *Cahier de la santé, 8*, Genève, DASS.
* Lalive d'Épinay, Christian; Maystre, Carole; Bickel, Jean-François; Hagmann, Hermann-Michel; Michel, Jean-Pierre; Riand, Jean-François. (1997). Un bilan de santé de la population âgée: comparaison entre deux régions de Suisse et analyse des changements sur quinze ans (1979-1994). *Cahiers médico-sociaux, 41*, 109-131.
* Lalive d'Épinay, Christian; Bickel, Jean-François; Maystre,Carole; Vollenwyder, Nathalie. (2000). *Vieillesses au fil du temps. 1979-1994. Une révolution tranquille*. Lausanne: Réalités sociales.
* Spini, Dario, Lalive d'Épinay, Christian. (2000). A comparison of attitudes towards self, life and society among Swiss elders between 1979 and 1994: Disentangling aging and sociocultural factors. *Comparer ou prédire: exemples de recherches comparatives en psychologie aujourd'hui*, Fribourg: Ed. universitaires, 177-186.
