# Mise à l'écart et dépendance des personnes âgées

«Mise à l'écart et dépendance des personnes âgées» est une enquête transversale sur la santé des personnes âgées en Valais et à Genève, réalisée en 1978-1979 par le GUGRISPA (Groupe universitaire genevois de recherche interdisciplinaire sur les personnes âgées).

Ce projet a été financé par le Fonds national suisse de la recherche scientifique ([FNS](http://www.snf.ch/)) à travers le Programme national de recherche no 3 (PNR 3): *Problèmes d'intégration sociale: les jeunes; les personnes âgées*.

## Pour commencer

Nous proposons ici un jeu de données mis à jour et corrigé en 2017, ainsi qu'une documentation complète. La liste des fichiers disponibles est [détaillée ci-dessous](#contenu), et la documentation se trouve dans le fichier [trans_1979_doc.md](trans_1979_doc.md).

Les données de l'enquête avaient été déposées auprès du SIDOS en 2005 et sont aujourd'hui [disponibles auprès de FORS](https://forsbase.unil.ch/project/study-public-overview/2458/0/), accompagnées de leur documentation de l'époque. L'établissement du jeu de données avait fait l'objet d'un travail important (voir la documentation qui est fournie par FORS), mais avait laissé de côté un certain nombre d'étapes que nous avons reprises en 2017.

En clonant ce dépôt, vous vous engagez à respecter la [licence d'utilisation](LICENSE) et à citer [les auteur·e·s](#auteures) et le titre de l'enquête ([voir la citation proposée](#citation)).

### Citation

(FR)
> Lalive d'Épinay, C. (dir.). (1979). *Mise à l'écart et dépendance des personnes âgées* [Jeu de données et documentation]. Tiré de https://gitlab.unige.ch/cigev/

(EN)
> Lalive d'Épinay, C. (Dir.). (1979). *Mise à l'écart et dépendance des personnes âgées* [Data file and documentation]. Retrieved from https://gitlab.unige.ch/cigev/

### Prérequis

Vous pouvez télécharger les données fournies par le CIGEV et les utiliser librement, selon la [licence d'utilisation](LICENSE). Afin de faire profiter les autres utilisateurs/trices de vos corrections éventuelles, utilisez Git: voir le livre [Pro Git](https://git-scm.com/book/fr/v2) pour bien commencer. Pour utiliser les commandes de Git, vous devez vous [créer une paire de clés SSH](https://git-scm.com/book/fr/v2/Git-sur-le-serveur-Génération-des-clés-publiques-SSH) et intégrer la clé publique à votre profil (Settings > SSH Keys).

Le [jeu de données](trans_1979_data.csv) est fourni au format [CSV](https://fr.wikipedia.org/wiki/Comma-separated_values), mais peut être importé dans [SPSS](https://fr.wikipedia.org/wiki/SPSS) grâce à la [syntaxe d'importation](trans_1979_data.sps) qui l'accompagne (modifiez la ligne appelant le fichier CSV en indiquant le chemin complet du fichier). La [documentation](trans_1979_doc.md) est fournie sous la forme d'un fichier [MD](https://fr.wikipedia.org/wiki/Markdown). Le [codebook](trans_1979_codebook.ods) a été exporté depuis SPSS et est fourni au format [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument). Les questionnaires ([1ère partie](trans_1979_quest_part1.pdf) et [2e partie](trans_1979_quest_part2.pdf)) ainsi qu'une version scannée du [rapport final](trans_1979_rapportfinal.pdf) sont au format [PDF](https://fr.wikipedia.org/wiki/Portable_Document_Format).


### Obtenir les données

Pour utiliser les données de ce projet, vous pouvez copier le jeu de données et la documentation en les téléchargeant depuis ce site (voir lien «Download» en haut à droite de la liste des fichiers) ou en clonant le dépôt:

```
git clone git@gitlab.unige.ch:cigev/trans_1979_gugrispa.git
```

Si vous souhaitez apporter des modifications à ce projet, il faut le *forker*, c'est-à-dire vous créer une copie des fichiers dans votre espace personnel, auxquels vous pourrez apporter des modifications. Vous pourrez par la suite partager ces modifications ou demander à la personne responsable de ce projet de les intégrer dans la branche principale. Utilisez pour cela le bouton «Fork» qui se trouve en haut de [la page principale](https://gitlab.unige.ch/cigev/trans_1979_gugrispa).

## Contenu

* [LICENSE](LICENSE) – Licence d'utilisation des données
* [README.md](README.md) – Indications de base (ce fichier)
* [trans_1979_codebook.ods](trans_1979_codebook.ods) – Codebook au format OpenDocument (feuille de calcul)
* [trans_1979_data.csv](trans_1979_data.csv) – Jeu de données au format CSV
* [trans_1979_data.sps](trans_1979_data.sps) – Syntaxe d'importation du jeu de données au format SPSS
* [trans_1979_doc.md](trans_1979_doc.md) – Documentation et rapport de traitement au format MD
* [trans_1979_quest_part1.pdf](trans_1979_quest_part1.pdf) – Questionnaire, 1ère partie (auto-administré)
* [trans_1979_quest_part2.pdf](trans_1979_quest_part2.pdf) – Questionnaire, 2e partie (face-à-face)
* [trans_1979_rapportfinal.pdf](trans_1979_rapportfinal.pdf) – Rapport final, tome 1

## Contribuer

Si vous désirez contribuer à l'amélioration de ce jeu de données, de sa documentation, ou laisser des commentaires, vous pouvez contacter le CIGEV à l'adresse: [cigev-data@unige.ch](mailto:cigev-data@unige.ch). Le système Git vous permettra de proposer des modifications que nous pourrons valider, documenter et partager.

Pour toute utilisation des données (analyse, comparaison, enseignement, publication...), nous vous serions reconnaissants de bien vouloir nous informer: [cigev-data@unige.ch](mailto:cigev-data@unige.ch).

## Auteur·e·s

* **Christian Lalive d'Épinay** – *Chef de projet*
* **Étienne Christe** – *Chercheur associé*
* **Josette Coenen-Huter** – *Chercheuse associée*
* **Hermann-Michel Hagmann** – *Co-requérant*
* **Olivier Jeanneret** – *Premier co-requérant*
* **Jean-Pierre Junod** – *Co-requérant*
* **Jean Kellerhals** – *Co-requérant*
* **Luc Raymond** – *Co-requérant*
* **Jean-Pierre Schellhorn** – *Co-requérant*
* **Geneviève Wirth** – *Chercheuse associée*
* **Bernard de Wurstenberger** – *Chercheur associé*

### Collaboratrices et collaborateurs

* **Alain Clémence**
* **Marianne Modak**
* **Marie-Noëlle La Spada-Schurmans**
* **Catherine Borrini**
* **Claudio Bolzman**
* **France von Allmen**
* **Dominique Froidevaux**
* **Isabelle Hirschi**
* **Emmanuel Lazega**

## Licence

Ce projet est distribué selon les termes de la licence de CreativeCommons CC-BY-SA. Voir le fichier [LICENSE](LICENSE) pour plus de détails.

## Remerciements

Merci à toutes les personnes (notamment secrétaires, enquêtrices et enquêteurs) qui ont également collaboré à cette recherche.
